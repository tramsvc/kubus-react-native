# Biodiesel Tracking App #

Master branch using RN 0.61.5

### What is this repository for? ###

*Quick summary will be added later.*

* Application Version
    * Stable: 1.0.1

### Requirements ###

* NodeJS 10 - 11.15 
    * [Download Node.js Here](https://nodejs.org/en/download/releases/)
* JDK 8
    * [Download JDK 8 Here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Python 2
    * [Download Python Here](https://www.python.org/downloads/)
* Android Studio with Android SDK Platform 28
    * [Download Android Studio Here](https://developer.android.com/studio)
* Device or Android Emulator with Android 4.4.2 or higher
* *iOS not tested yet*

### Setup and Running ###

* Fork this repo
* Open terminal and change directory to project root 
* run ``npm install`` to install project dependencies
* Run project using following command
    * Run Application in debug build  ``npx react-native run-android``
    * Run Application in release build  ``npx react-native run-android --variant=release``

    **_(Android)_ Keystore is required to generate release build.** 
    
    More info about generating keystore and release build configuration can be found [here](https://facebook.github.io/react-native/docs/signed-apk-android)

### Project Maintainer ###

* Narawit Khammamart

### Readme Version ###
* 0.1.2   Update application version, Remove unnecessary information.
* 0.1.1   Edit run release build instruction 
* 0.1     Initial