# แอพพลิเคชั่นติดตามรถโดยสารไบโอดีเซล #

แอพพลิเคชั่นสำหรับติดตาม, ค้นหารถโดยสารไบโอดีเซล สำหรับมหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตกำแพงแสน

Master Branch ใช้ React-Native v. 0.61.5

* เวอร์ชั่นของแอพพลิเคชั่น
    * master: 1.0.1

### Repositories อื่นที่ใช้ร่วมกันงานนี้ ###
https://bitbucket.org/account/user/tramsvc/projects/KUB

### สิ่งที่ต้องใช้ในการพัฒนาแอพพลิเคชั่น ###

#### Note: เนื่องจากผู้พัฒนาใช้ Windows ถ้าใช้ Mac ไปคลำทางตามนี้ [เลย](https://facebook.github.io/react-native/docs/getting-started) ####

* NodeJS 10 - 11.15 
    * [ดาวน์โหลด Node.js ได้ที่นี่](https://nodejs.org/en/download/releases/)
* JDK 8
    * [ดาวน์โหลด Node.js ได้ที่นี่](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (เนื่องจาก JDK 8 EOL แล้ว จำเป็นต้อง Login ด้วยบัญชี Oracle  เพื่อดาวน์โหลด)
* Python 2
    * [ดาวน์โหลด Python ได้ที่นี่](https://www.python.org/downloads/)
* Android Studio with Android SDK Platform 28
    * [ดาวน์โหลด Android Studio ได้ที่นี่](https://developer.android.com/studio)
* Android 4.4.2 หรือสูงกว่า
* iOS 9.0+ *(ยังไม่ได้ทดสอบบน iOS เพราะ ไม่มี Mac)*

### การติดตั้งและการ Run Application ###

* Fork หรือ Clone repo นี้
* เปิด Terminal หรือ cmd ที่ถนัด จากนั้นเปลี่ยน Directory ไปที่ root ของ project 
* พิมพ์คำสั่ง ``npm install`` เพื่อดาวน์โหลด Project dependencies
* รันแอพตามคำสั่งต่อไปนี้
    * **สร้าง Debug build:**  ``npx react-native run-android``
    * **(Debug)** หากอุปกรณ์ที่ใช้งานมีแอพที่ build แล้ว สามารถรัน Metro เพื่ออัปเดตแอพได้เลย: ``npx react-native start``
    * **สร้าง Release build:**  ``npx react-native run-android --variant=release``

    **_(Android)_ ถ้าจำสร้าง Release Build จำเป็นต้อง Gen Keystore** 
    
   ข้อมูลเพิ่มเติมเกี่ยวกับการสร้าง Keystore และการตั่งค่าเพื่อสร้าง Release build สามารถอ่านได้จาก [ที่นี่](https://facebook.github.io/react-native/docs/signed-apk-android)

### Project Maintainer ###

* นราวิชญ์ คำมะนาถ

### Readme Version ### 
* 0.1     Init README_TH

___