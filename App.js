import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//import { createAppContainer } from 'react-navigation';
//import { createStackNavigator } from 'react-navigation-stack';
import { enableScreens } from 'react-native-screens';

//Import View class for use in navigation stack below.
import HomeScreen from 'components/home-screen';
import MapScreen from 'components/map-screen';
import AllBusMap from 'components/all-bus-map';
import QRScannerScreen from 'components/qr-scanner';
//import MarkerSimulation from 'components/markerSim';
//import StationInRouteList from 'components/station-in-route-list';
import BusRouteImage from 'components/route-image';

//Test Sec
//import GPSAccuracyTest from 'test-components/gps-with-map';

enableScreens();

const defaultScreenOption = {
  headerStyle: { backgroundColor: '#1ebdaf' },
  headerTintColor: '#fff',
  headerTitleStyle: { fontWeight: 'bold' }
};
const transparentHeader = { title: null, headerTransparent: true, headerStyle: { backgroundColor: 'transparent' }, headerTintColor: '#000' }
const blackHeader = { title: null, headerStyle: { backgroundColor: '#000' } };

const Stack = createStackNavigator();
function AppNavigator() {
  return (
    <Stack.Navigator initialRouteName="Home" screenOptions={defaultScreenOption}>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Map"
        component={MapScreen}
        options={transparentHeader}
      />
      <Stack.Screen
        name="QRScanner"
        component={QRScannerScreen}
        options={{ title: 'ค้นหาโดยการสแกน QRCode' }}
      />
      <Stack.Screen
        name="BusRouteImage"
        component={BusRouteImage}
        options={blackHeader}
      />
      <Stack.Screen
        name="AllBusMap"
        component={AllBusMap}
        options={transparentHeader}
      />
    </Stack.Navigator>
  )
}

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <AppNavigator />
      </NavigationContainer>
    )
  }
}
