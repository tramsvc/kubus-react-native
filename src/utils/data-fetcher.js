/* eslint-disable no-useless-catch */
import axios from 'axios';
import config from 'config/config';

const AXIOS_INSTANCE = axios.create({
    baseURL: `http://${config.server.address}/kubus/api/${config.api.version}`,
    timeout: config.server.connection_timeout,
});

/*
* Obsolete version 1 API
* Handle uri / type
*/
export async function GetData(mode, param = null) {
    let requestString = null;
    if(param !== null){
        requestString = `/?mode=${mode}&param=${param}`;
    }
    else{
        requestString = `/?mode=${mode}`;
    }
    try{
        const response = await AXIOS_INSTANCE.get(requestString);
        return response.data;
    }
    catch(e){
        throw e;
    }
}
export async function GetBusLocation() {
    const response = await GetData(3);
    return response;
}
export async function GetBusInRoute(param) {
    const response = await GetData(4, param);
    return response;
}

export async function GetAllStationsData(){
    const response = await GetData(6);
    return response;
}
export async function GetStationsDataInRoute($param){
    const response = await GetData(7,$param);
    return response;
} 

export async function GetStationLocation() {
    const response = await GetData(11);
    return response;
}
export async function GetWaypointInRoute(param) {
    const response = await GetData(10, param)
    return response;
}
export async function GetRouteInfo() {
    const response = await GetData(8);
    return response;
}
export async function GetRouteInfoByRouteID(param) {
    const response = await GetData(9, param);
    return response;
}
export async function GetRouteInStation(param) {
    const response = await GetData(12, param);
    return response;
}
export async function GetRouteAndStationDataForQRPage(stationID) {
    const response = await GetData(13, stationID);
    return response;
}

/*
export async function GetData(request, param = null){
    try{
        const response = await AXIOS_INSTANCE.get(`/${request}/${param}`);
        return response.data;
    }
    catch(e){
        throw e;
    }
}

export async function GetBusLocation() {
    const response = await GetData('buslocation');
    return response;
}
export async function GetBusInRoute(param) {
    const response = await GetData('buslocation-in-route', param);
    return response;
}
export async function GetStationLocation() {
    const response = await GetData('waypoint');
    return response;
}
export async function GetWaypointInRoute(param) {
    const response = await GetData('waypoint-in-route', param);
    return response;
}
export async function GetRouteInfo() {
    const response = await GetData('route');
    return response;
}
export async function GetRouteInfoByRouteID(param) {
    const response = await GetData('route', param);
    return response;
}
export async function GetRouteInStation(param) {
    const response = await GetData('route-in-station', param);
    return response;
}
//Route On Station
export async function GetRouteAndStationDataForQRPage(stationID) {
    const response = await GetData('station-in-route', stationID);
    return response;
}
*/
