export default function TryParseJSON(data) {
  try {
    let obj = JSON.parse(data);
    if (isObject(obj))
      return obj;
  }
  catch (e) {
    return false;
  }
}

function isObject(obj) {
  if (obj !== undefined && obj !== null && obj.constructor == Object && Object.prototype.hasOwnProperty.call(obj, "id"))
    return true;
  else
    return false;
}
