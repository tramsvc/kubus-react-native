export default function GenerateLatLng(latMinBound, latMaxBound, lngMinBound, lngMaxBound) {
	let latMin = latMinBound;
	let latMax = latMaxBound;
	let lngMin = lngMinBound;
	let lngMax = lngMaxBound;
	return {lat: (Math.random() * (latMax - latMin)) + latMin, lng: (Math.random() * (lngMax - lngMin)) + lngMin}
}