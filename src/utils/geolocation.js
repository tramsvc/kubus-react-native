//Maybe Deprecrate will be remove later.
import Geolocation from 'react-native-geolocation-service';
import { requestLocationPermission } from 'utils/requestPermission';

export function getGeolocationOnce() {
  let geolocationModel;
  requestLocationPermission();
  try {
    Geolocation.getCurrentPosition((position) => {
        geolocationModel = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }
      return geolocationModel;
    },(error)=>{
      throw error
    },
      { enableHighAccuracy: false, timeout: 5000, maximumAge: 3000 }
    );
  }
  catch (error) {
    return error;
  }
}