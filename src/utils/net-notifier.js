import React, { Component } from 'react';
import NotifierView from 'utils/notifier/notifier-view';
import NetInfo from "@react-native-community/netinfo";

export default class NetNotifier extends Component {
    constructor(props) {
        super(props);
        this.state = {
            connectionType:'none',
            isInternetReachable:false,
            hasConnection: false
        };
        this._netInfoSubscription;
    }
    componentDidMount() {
        this._netInfoSubscription = NetInfo.addEventListener(state => {
            this.setState({ hasConnection: state.isConnected, connectionType: state.type, isInternetReachable: state.isInternetReachable })
        });
    }
    componentWillUnmount() {
        this._netInfoSubscription();
    }

    renderInfoWindow() {
        const {hasConnection, connectionType, isInternetReachable} = this.state;
        if (hasConnection && (connectionType !== 'none' && connectionType !== 'unknown') && isInternetReachable) {
            return null;
        }
        else if(hasConnection && (connectionType !== 'none' && connectionType !== 'unknown') && !isInternetReachable) {
            return this.renderOfflineNotifier('This connection has no internet access.');
        }
        else{
            return this.renderOfflineNotifier('No internet connection.');
        }
    }
    renderOfflineNotifier(text) {
        return <NotifierView displayText={text} />
    }
    render() {
        return this.renderInfoWindow();
    }
}