//Route Mapper Here.
export function MapSelectRouteRadioOption(data) {
    const routeData = data;
    const DATA_LENGTH = routeData.length;
    let radioOptions = [];
    for (let i = 0; i < DATA_LENGTH; i++)
        radioOptions.push({ label: `${routeData[i].route_name} - ${routeData[i].route_description}`, value: routeData[i].route_id });
    radioOptions.push({ label: 'ทุกสาย', value: -1 });
    return radioOptions;
}

//QRCode Scanner Route mapper then send routeData to OptionMapper
export function QRMapSelectRouteRadioOption(data) {
    let queryData = data;
    const DATA_LENGTH = queryData.length;
    let routeData = [];
    if (Array.isArray(queryData)) {
        for (let i = 0; i < DATA_LENGTH; i++)
            routeData[i] = { 'route_id': queryData[i].route_id, 'route_name': queryData[i].route_name, 'route_description': queryData[i].route_description };
    }
    return MapSelectRouteRadioOption(routeData);
}