export function TimeCalc(distance, speed) {
	let _speed = speed;
	if(speed > 5 || speed < 25){
		_speed = 25;
	}
	return (distance / _speed) * 3600;
}

export function TimeFormatter(timeInSecs) {
	if (timeInSecs === Infinity) {
		return '∞';
	}
	else if (timeInSecs < 60) {
		return 'น้อยกว่า 1 นาที';
	}
	else {
		let hrs = Math.floor(timeInSecs / 3600);
		let mins = Math.floor((timeInSecs % 3600) / 60);
		let secs = Math.floor(timeInSecs % 60);

		let result = "";
		if (timeInSecs >= 0) {
			if (hrs > 0) {
				result +=  hrs + " ชั่วโมง"
				result += " " + mins + " นาที" + " " + secs + " วินาที"
			}
			else {
				result += mins + " นาที" + " " + secs + " วินาที";
			}
		}
		else {
			return result += "0 วินาที";
		}
		return result;
	}
}
