import Polyline from "@mapbox/polyline";

export function DecodePolyline(directionData, routes = null) {
    try {
        if (routes) {
            const route = parseInt(routes);
            let points = Polyline.decode(directionData[route - 1].overview_polyline.points);
            let coords = points.map(function(point){
                return {
                    latitude: point[0],
                    longitude: point[1]
                }
            });
            coords.color = directionData[route-1].color;
            coords.routeType = 'single';
            return coords;
        }
        else {
            let polylineData = [];
            directionData.forEach(function(element){
                let obj = {};
                let points = Polyline.decode(element.overview_polyline.points);
                obj.coords = points.map(function(point){
                    return {
                        latitude: point[0],
                        longitude: point[1]
                    };
                });
                obj.color = element.color;
                polylineData.push(obj);            
            });
            polylineData.routeType = 'multiple';
            return polylineData;
        }

    }
    catch (error) {
        return error;
    }
}