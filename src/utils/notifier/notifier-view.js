import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

export default function NotifierView({displayText}){
    return (
        <View style={styles.container}>
            <Text style={styles.infoText}>{displayText}</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        height: 20,
        width:'100%',
        alignItems: 'center',
        backgroundColor: 'red',
    },
    infoText: {
        color: '#FFF'
    }
});