import { PermissionsAndroid, Alert } from 'react-native';

export async function CheckCameraPermissionGranted() {
    const isPermissionGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
    return isPermissionGranted;
}
export async function RequestCameraPermission() {
    try {
        const granted = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.CAMERA
        );
        if (granted) {
            return granted;
        }
        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
                title: 'Requesting Camera Permission.',
                message: 'ฟังก์ชั่นนี้ต้องการสิทธิ์ในการใช้กล้องถ่ายรูปของคุณ เพื่อทำการสแกน QRCode ที่สถานี จะได้ทราบว่าคุณอยู่สถานีใด. ',
                buttonPositive: 'อนุญาต',
                buttonNegative: 'ไม่อนุญาต'
            },
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) {
            return true
        }
        return false;

    } catch (err) {
        Alert.alert('Error', err.message, [{ text: 'OK' }]);
    }
}

export async function requestLocationPermission() {
    try {
        const granted = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );
        if (granted) {
            return granted;
        }
        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: 'Requesting Fine Location Permission.',
                message: 'ฟังก์ชั่นนี้ต้องการใช้งาน GPS บนอุปกรณ์ของคุณ เพื่อใช้ในการระบุตำแหน่งของคุณบนแผนที่.',
                buttonPositive: 'อนุญาต',
                buttonNegative: 'ไม่อนุญาต'
            },
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) {
            return true
        }
        return false;

    } catch (err) {
        Alert.alert('Error', err.message, [{ text: 'OK' }]);
    }
}