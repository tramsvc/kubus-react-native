import React, { PureComponent } from 'react';
import NotifierView from 'utils/notifier/notifier-view';

export default class ErrorNotifier extends PureComponent {
    constructor(props) {
        super(props);
        this._intervalID = null;
        this.state = {
            showNotifier: false,
            errorMessage: null,
        };
    }
    componentWillUnMount() {
        if (typeof (this._intervalID) !== 'undefined')
            clearTimeout(this._intervalID);
    }
    componentDidUpdate(prevProps) {
        if (prevProps.errorMessage !== this.props.errorMessage && this.props.errorMessage !== null) {
            this.setState({ showNotifier: true, errorMessage: this.props.errorMessage });
            this._intervalID = setTimeout(() => {
                this.setState({ showNotifier: false, errorMessage: null });
            }, 5000);
        }
    }
    _renderErrorNotifier() {
        const { showNotifier, errorMessage } = this.state;
        if (showNotifier) {
            return this._renderErrorWindow(errorMessage);
        }
        else {
            return null;
        }
    }
    _renderErrorWindow(text) {
        return <NotifierView displayText={text} />
    }
    render() {
        return this._renderErrorNotifier();
    }
}