import { TimeFormatter, TimeCalc } from 'utils/time';
import config from 'config/config';

export function IsUserNearUniversity(user_coordinate) {
    const KUKPS_COORDINATE = { 'latitude': config.map.KU_Lat, 'longitude': config.map.KU_Lon };
    const distance = CalculateDistance(user_coordinate.latitude, user_coordinate.longitude, KUKPS_COORDINATE.latitude, KUKPS_COORDINATE.longitude);
    if (distance <= 5) {
        return true;
    }
    else {
        return false;
    }
}

export function SnapToRoad(busData, polylineData) {
    let start = busData;
    let end = polylineData;
    let closest;
    let closest_distance;
    let temp;
    closest_distance = CalculateDistance(busData[0].latitude, busData[0].longitude, end[0].latitude, end[0].longitude, "M");
    closest = end[0];
    for (let i = 0; i < busData.length; i++) {
        for (let j = 0; j < end.length; j++) {
            if ((temp = CalculateDistance(busData[i].latitude, busData[i].longitude, end[j].latitude, end[j].longitude, "M")) <= closest_distance) {
                closest = end[j];
                closest_distance = temp;
            }
        }
        if (closest_distance >= 3 && closest_distance <= 10) {
            start[i].latitude = closest.latitude;
            start[i].longitude = closest.longitude;
        }
        if (i < busData.length - 1) {
            closest_distance = CalculateDistance(busData[i + 1].latitude, busData[i + 1].longitude, end[0].latitude, end[0].longitude, "M");
            closest = end[0];
            temp = null;
        }
    }
    return start;
}

//PASS
export function FindUserNearestStation(stations, user_position, selected_route) {
    let stationNearUser = FindClosestCoordinate(stations, user_position, selected_route);
    stationNearUser = FilterMultipleRouteInStation(stationNearUser, stations);
    return stationNearUser;
}
//PASS
export function FindNearestBusByStations(station_near_user, buses, selected_route) {
    try {
        let nearestBus = null;
        let busData = buses.filter(function (bus) { if (bus.is_active) return bus; });
        station_near_user = _sortBusByRoute(station_near_user);
        if (Array.isArray(station_near_user) && station_near_user.length > 1) {
            //console.log("Multiple Route");
            nearestBus = FindClosestBusEveryRoute(busData, station_near_user);
        }
        else {
            //console.log("Single Route");
            station_near_user = station_near_user[0];
            nearestBus = FindClosestCoordinate(busData, station_near_user, selected_route);
        }
        /* Moved to main map-screen init-state
        const waitTime = TimeFormatter(TimeCalc(nearestBus.distance, nearestBus.speed));
        nearestBus['waitTime'] = waitTime;
        nearestBus['displayDistance'] = ConvertKiloToMeters(nearestBus.distance);
        */
        return nearestBus;
    }
    catch (e) {
        return null;
    }
}
//PASS
/*
export function FindNearestBusByUserLocation(targetBus, userCoordinate) {
    try {
        //let busData = Array.isArray(targetBus) ? targetBus.filter(function (bus) { if (bus.is_active) return bus }) : targetBus;
        let busData = 
        let tappedBus = FindClosestCoordinate(busData, userCoordinate);
        let waitTime = TimeFormatter(TimeCalc(tappedBus.distance, tappedBus.speed));
        tappedBus['waitTime'] = waitTime;
        tappedBus.displayDistance = ConvertKiloToMeters(tappedBus.distance);
        return tappedBus;
    }
    catch (e) {
        return null;
    }
}
*/

export function FindClosestBusEveryRoute(start, end) {
    //Start = Bus, End = คน
    let bus_filterArray = start;
    let endpoint = end;
    if (Array.isArray(endpoint)) {
        endpoint = endpoint[0];
    }
    bus_filterArray = FindClosestCoordinate(bus_filterArray, endpoint);
    return bus_filterArray;
}
//PASS
export function FilterMultipleRouteAndStepInStation(target, stations) {
    let stations_array = FilterMultipleRouteInStation(target, stations);
    if (stations_array.length > 1) {
        _FilterSameStationInRoute(stations_array);
    }
    return stations_array;
}

export function FilterMultipleRouteInStation(targetStation, Stations) {
    let newArray = Stations.filter(function (element) {
        if (element.station_id === targetStation.station_id) {
            return element;
        }
    });
    return newArray;
}

function _FilterSameStationInRoute(FilterMultipleRouteInStationArray) {
    for (let i = 1; i < FilterMultipleRouteInStationArray.length; i++) {
        if (FilterMultipleRouteInStationArray[i].route_id === FilterMultipleRouteInStationArray[i - 1].route_id)
            FilterMultipleRouteInStationArray.splice(i, 1);
    }
}

//PASS
export function FilterBusStep(busData, userStep, stepData, selectedRoute) {
    let filteredArray = busData.filter(function (element) {
        for (let i = 0; i < userStep.length; i++) {
            if ((element.is_active) && (element.step < userStep[i].step || element.step === stepData.length) && (element.route_id === selectedRoute)) {
                return element;
            }
            else if ((element.step === userStep[i].step) && (element.route_id === selectedRoute)) {
                let distance = CalcDistInKiloMeters(element.latitude, element.longitude, userStep[i].latitude, userStep[i].longitude);
                if (distance < 0.025) {
                    return element;
                }
            }
        }
    });
    return filteredArray;
}

//PASS
function _sortBusByRoute(buses) {
    let busData = buses;
    busData.sort(function (a, b) {
        return a.route_id - b.route_id;
    });
    return busData;
}
//PASS
export function ConvertKiloToMeters(kiloValue) {
    if (kiloValue < 1) {
        let convertedValue = kiloValue * 1000;
        return `${convertedValue.toFixed(2)} m.`;
    }
    return `${kiloValue.toFixed(2)} km.`;
}

//Route related functions.

//PASS
export function FindClosestCoordinate(start, end, route_filter = null) {
    let startArray = start;
    let closest;
    let closest_dist;
    let temp;
    try {
        if (!Array.isArray(startArray)) {
            closest = start;
            closest_dist = CalculateDistance(start['latitude'], start['longitude'], end.latitude, end.longitude);
            closest['distance'] = closest_dist;
            return closest;
        }
        else if (startArray.length < 2) {
            closest = startArray[0];
            closest_dist = CalculateDistance(startArray[0]['latitude'], startArray[0]['longitude'], end.latitude, end.longitude);
            closest['distance'] = closest_dist;
            return closest;
        }
        else {
            //No step, No route filter
            if (route_filter === null) {
                closest = startArray[0];
                closest_dist = CalculateDistance(startArray[0]['latitude'], startArray[0]['longitude'], end.latitude, end.longitude);
                for (let index = 1; index < startArray.length; index++) {
                    if ((temp = CalculateDistance(startArray[index]['latitude'], startArray[index]['longitude'], end.latitude, end.longitude)) < closest_dist) {
                        closest_dist = temp;
                        closest = startArray[index];
                    }
                }
            }
            //Filter route without step.
            else if (route_filter !== null) {
                if (startArray.length === 0) { throw new Error("There's no bus near your location or route is invalid.") }
                else if (startArray.length === 1) {
                    closest = startArray[0];
                    closest_dist = CalculateDistance(startArray[0]['latitude'], startArray[0]['longitude'], end.latitude, end.longitude);
                }
                else {
                    closest = startArray[0];
                    closest_dist = CalculateDistance(startArray[0]['latitude'], startArray[0]['longitude'], end.latitude, end.longitude);
                    for (let index = 1; index < startArray.length; index++) {
                        if ((temp = CalculateDistance(startArray[index]['latitude'], startArray[index]['longitude'], end.latitude, end.longitude)) < closest_dist) {
                            closest_dist = temp;
                            closest = startArray[index];
                        }
                    }
                }
            }

        }
        closest['distance'] = closest_dist;
        return closest;
    }
    catch (error) {
        return error;
    }
}

/*
* Calculation section
* Spherical law of cos
*/
export function CalculateDistance(lat1, lon1, lat2, lon2, unit = "K") {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        let radlat1 = Math.PI * lat1 / 180;
        let radlat2 = Math.PI * lat2 / 180;
        let theta = lon1 - lon2;
        let radtheta = Math.PI * theta / 180;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI; //todeg
        dist = dist * 60 * 1.1515;
        //param: M = Meters, K = Kilometers, N = Nautical Miles
        if (unit === "M") { dist *= 1609.344 }
        if (unit === "K") { dist *= 1.609344 }
        if (unit === "N") { dist *= 0.8684 }
        return dist;
    }
}

export function CalcAvgSpeed(startTime, totalDistance) {
    const currentTime = new Date();
    let diffTime = currentTime - startTime;
    diffTime = diffTime / (3600 * 1000);
    let avgSpeed = totalDistance / diffTime;
    return avgSpeed;
}

export function CalcDistInKiloMeters(lat1, lon1, lat2, lon2, unit = 'K') {
    let R = 6371; // Radius of the earth in km.
    let degLat1 = deg2rad(lat1);
    let degLat2 = deg2rad(lat2);
    let dLat = deg2rad(lat2 - lat1);  // deg2rad below
    let dLon = deg2rad(lon2 - lon1);
    let a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(degLat1) * Math.cos(degLat2) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    if (unit === "M") { d *= 1000 }
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

export function FindClosestCoordinateByNode(polylineData, decodedPolyline, route, stationStep, busData) {
    let nearest = busData[0];
    let tempCalcBusData = CalcBusDistanceByNode(polylineData, decodedPolyline, route, busData[0].step, stationStep[0].step, busData[0]);
    let minDist = tempCalcBusData.distance;
    nearest['distance'] = minDist;
    nearest['nearestStationIndex'] = 0;
    nearest['targetPolyline'] = tempCalcBusData.targetPolyline;
    for (let i = 0; i < stationStep.length; i++) {
        for (let j = 1; j < busData.length; j++) {
            tempCalcBusData = CalcBusDistanceByNode(polylineData, decodedPolyline, route, busData[j].step, stationStep[i].step, busData[j]);
            // Disable this part when don't want to track same step car.
            if (busData[j].step === stationStep[i].step)
                continue;
            if (tempCalcBusData.distance < minDist) {
                minDist = tempCalcBusData.distance;
                nearest = busData[j];
                nearest['distance'] = minDist;
                nearest['targetPolyline'] = tempCalcBusData.targetPolyline;
                nearest['nearestStationIndex'] = i;
            }
        }
    }
    return nearest;
}

export function CalcBusDistanceByNode(polylineData, decodedPolyline, route, startStep, endStep, targetBusData = null) {
    let dist = 0;
    let result_obj = null;
    let polylineArray = [];
    let temp = null;
    let returningTrip = false;
    let startBound = null;
    let endBound = null;
    const findRouteIndex = (element) => element.route_id === route;
    const routeIndex = polylineData.findIndex(findRouteIndex);
    if (targetBusData) {
        const result = FindMiddleIndex(polylineData, routeIndex, decodedPolyline, targetBusData);
        if (result.nearestNode !== null)
            startBound = result.nearestNode;
        else
            //Will be improvise later (CASE: Car location don't match with step)
            startBound = polylineData[routeIndex].stepBound[startStep - 1].start;
    }
    else {
        startBound = polylineData[routeIndex].stepBound[startStep - 1].start;
        endBound = polylineData[routeIndex].stepBound[endStep - 1].start;
    }
    if (startStep > endStep) {
        returningTrip = true;
        const lastStepIndex = polylineData[routeIndex].stepBound.length;
        endBound = polylineData[routeIndex].stepBound[lastStepIndex - 1].end;
    }
    else if (startStep === endStep) {
        startBound = polylineData[routeIndex].stepBound[startStep - 1].start;
        const tempEndBound = FindMiddleIndex(polylineData, routeIndex, decodedPolyline, targetBusData).nearestNode;
        endBound = tempEndBound;
    }
    else {
        endBound = polylineData[routeIndex].stepBound[endStep - 1].start;
    }
    for (let i = startBound; i < endBound; i++) {
        temp = CalcDistInKiloMeters(decodedPolyline[i].latitude, decodedPolyline[i].longitude, decodedPolyline[i + 1].latitude, decodedPolyline[i + 1].longitude);
        polylineArray.push({ 'latitude': decodedPolyline[i].latitude, 'longitude': decodedPolyline[i].longitude }, { 'latitude': decodedPolyline[i + 1].latitude, 'longitude': decodedPolyline[i + 1].longitude });
        dist += temp;
        if (returningTrip && i === endBound - 1) {
            returningTrip = false;
            i = 0;
            endBound = polylineData[routeIndex].stepBound[endStep - 1].start;
        }
    }
    result_obj = { "targetPolyline": polylineArray, "distance": dist };
    return result_obj;
}

function FindMiddleIndex(PolylineData, indexRoute, decodePolyline, busLocationData) {
    let distance = 0;
    let tempDist = 0;
    let nearestNode = null;
    const startStep = busLocationData.step;
    let result = { "nearestNode": null, "distance": null };
    const startIndex = PolylineData[indexRoute].stepBound[startStep - 1].start;
    const endIndex = PolylineData[indexRoute].stepBound[startStep - 1].end;
    nearestNode = startIndex;
    distance = CalcDistInKiloMeters(busLocationData.latitude, busLocationData.longitude, decodePolyline[startIndex].latitude, decodePolyline[startIndex].longitude);
    for (let i = startIndex; i <= endIndex; i++) {
        if ((tempDist = CalcDistInKiloMeters(busLocationData.latitude, busLocationData.longitude, decodePolyline[i].latitude, decodePolyline[i].longitude)) <= distance) {
            distance = tempDist;
            nearestNode = i;
        }
    }
    result.nearestNode = nearestNode;
    result.distance = distance;
    return result;
}
