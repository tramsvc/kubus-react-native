import React, { Component } from 'react';
import { Button, View, Dimensions, Alert, Text, FlatList } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Ripple from 'react-native-material-ripple';
import { styles } from 'styles/station-in-route-list';
import { GetData } from 'utils/data-fetcher';
import { CalculateDistance } from 'utils/new-routing-function';
import { requestLocationPermission } from 'utils/permission';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 14.0236026;
const LONGITUDE = 99.9748993;
const LATITUDE_DELTA = 0.0622;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const KU_Region = { latitude: LATITUDE, longitude: LONGITUDE, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA };
//const INTERVAL_TIME = 2000;

export default class GPSAccuracyTest extends Component {
    constructor(props) {
        super(props);
        this.IntervalID = null;
        this.watchId = null;
        this.screenMounted = true;
        //this.IntervalMarkerUpdate = this.IntervalMarkerUpdate.bind(this);
        //this._onMapReadyInit = this._onMapReadyInit.bind(this);
        this.state = {
            recorded: [],
            stations: null,
            arduinoMarker: null,
            coordinate: { latitude: 0, longitude: 0 }
        };
    }

    componentDidMount() {
        requestLocationPermission();
        this.UpdateLocationByGPS();
        //this.updateLocation();
    }

    componentWillUnmount() {
        this.screenMounted = false;
        if (this.watchId !== null) 
            Geolocation.clearWatch(this.watchId);
    }
    /*
    _onMapReadyInit() {
        //this._setIntervalUpdate(INTERVAL_TIME);
    }
    */
    /*
    _setIntervalUpdate(intervalTime) {
        this.IntervalID = setTimeout(this.IntervalMarkerUpdate, intervalTime, intervalTime);
    }
    
    _clearIntervalUpdate() {
        clearTimeout(this.IntervalID);
        this.IntervalID = undefined;
    }
    
    async IntervalMarkerUpdate(intervalTime) {
        try {
            if (this.screenMounted) {
                await this.updateLocation();
                this._setIntervalUpdate(intervalTime);
            }
        }
        catch (e) {
            this._RetryConnection(e);
        }
    }
    

    async updateLocation() {
        let arduino = null;
        arduino = await GetData(5, 999);
        this.setState({ arduinoMarker: arduino });
    }
    _RetryConnection = () => {
        this._setIntervalUpdate(INTERVAL_TIME);
    }
 */
    UpdateLocationByGPS() {
        this.watchId = Geolocation.watchPosition(
            async (position) => {
                let arduino = null;
                arduino = await GetData(5, 999);
                this.setState({
                    arduinoMarker: arduino,
                    coordinate: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                });
            },
            (error) => Alert.alert('A-GPS Error', error.message, [{ text: 'OK' }]),
            { enableHighAccuracy: true, distanceFilter: 0, interval: 2000, fastestInterval: 2000 }
        );
    }

    _saveCoordinate = () => {
        if (this.state.arduinoMarker && this.state.coordinate) {
            const recordCount = this.state.recorded.length;
            const dist = CalculateDistance(this.state.coordinate.latitude, this.state.coordinate.longitude, this.state.arduinoMarker.latitude, this.state.arduinoMarker.longitude, 'M');
            let prep = { id: recordCount + 1, arduino: { latitude: this.state.arduinoMarker.latitude, longitude: this.state.arduinoMarker.longitude }, phone: { latitude: this.state.coordinate.latitude, longitude: this.state.coordinate.longitude }, distance: dist, timestamp: this._genDate() };
            this.setState({ recorded: [prep, ...this.state.recorded] });
        }

    }
    _genDate = () => {
        let currentdate = new Date();
        let datetime = "Last Save: " + currentdate.getDate() + "/"
            + (currentdate.getMonth() + 1) + "/"
            + currentdate.getFullYear() + " @ "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
        return datetime;
    }
    _renderCoordinateText() {
        if (this.state.coordinate !== null)
            return <Text>A-GPS Current Coordinate: {this.state.coordinate.latitude}, {this.state.coordinate.longitude}</Text>
        else {
            return null;
        }
    }
    _renderArduinoCoordinate() {
        if (this.state.arduinoMarker !== null)
            return <Text>Arduino Current Coordinate: {this.state.arduinoMarker.latitude}, {this.state.arduinoMarker.longitude}</Text>
        else
            return null;
    }
    _renderSeparator() {
        return (
            <View style={styles.separator} />
        );
    }
    _renderEmptyListText() {
        return <Text style={styles.emptyListText}>ไม่มีการบันทึกพิกัด</Text>;
    }
    _renderArduinoMarker() {
        if (this.state.arduinoMarker) {
            return <Marker
                title='IteadStudio GPS'
                pinColor={'green'}
                description={`Update Time: ${this.state.arduinoMarker.timestamp}`}
                coordinate={{ latitude: this.state.arduinoMarker.latitude, longitude: this.state.arduinoMarker.longitude }}
            />
        }
        else
            return null;
    }
    _renderPhoneMarker() {
        if (this.state.coordinate === null) {
            return null;
        }
        else {
            return (
                <Marker
                    title='Phone Marker'
                    description={`${this.state.coordinate.latitude}, ${this.state.coordinate.longitude}`}
                    coordinate={this.state.coordinate}
                />
            );

        }
    }

    render() {
        return (
            <View style={styles.container}>
                <MapView
                    style={{ height: 250 }}
                    followsUserLocation={true}
                    initialRegion={KU_Region}
                    provider={PROVIDER_GOOGLE}
                    zoomControlEnabled={true}
                //onMapReady={this._onMapReadyInit}
                >
                    {this._renderPhoneMarker()}
                    {this._renderArduinoMarker()}
                </MapView>
                <View style={{ marginVertical: 10 }}>
                    {this._renderArduinoCoordinate()}
                    {this._renderCoordinateText()}
                    <Button onPress={this._saveCoordinate} title='Save Coordinate to list' />
                </View>
                <View style={styles.flatList}>
                    <FlatList
                        ItemSeparatorComponent={this._renderSeparator}
                        ListEmptyComponent={this._renderEmptyListText}
                        data={this.state.recorded}
                        renderItem={({ item }) =>
                            <Item
                                key={item.id}
                                order={item.id}
                                distance={item.distance}
                                arduinoCoordinate={item.arduino}
                                phoneCoordinate={item.phone}
                                timestamp={item.timestamp}
                            />
                        }
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </View>
        );
    }
}

class Item extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { order, timestamp, arduinoCoordinate, phoneCoordinate, distance } = this.props;
        return (
            <Ripple>
                <View style={styles.item}>
                    <View style={styles.textContainer}>
                        <Text>ID: {order}</Text>
                        <Text style={{ fontWeight: 'bold' }}>Phone Coordinate: {phoneCoordinate.latitude}, {phoneCoordinate.longitude}</Text>
                        <Text style={{ fontWeight: 'bold' }}>Arduino Coordinate: {arduinoCoordinate.latitude}, {arduinoCoordinate.longitude}</Text>
                        <Text>Dist Difference: {distance}</Text>
                        <Text>{timestamp}</Text>
                    </View>
                </View>
            </Ripple>
        )
    }
}