import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';

function BusMarkerFactory(props) {
    return (
        <Image
            backgroundColor={props.color}
            borderColor={props.color}
            borderRadius={50}
            borderWidth={2}
            source={require('assets/markersIcon/directional/arrow_drop_up.png')}
            transform={[{ rotateZ: `${props.courseDeg}deg` }]}

        />
    )
}
BusMarkerFactory.defaultProps = {
    courseDeg: '0'
}
BusMarkerFactory.propTypes = {
    courseDeg: PropTypes.number.isRequired
}

/*
function DirectionalTriangle(props) {
    return (
        <View style={{
            flex:1,
            backgroundColor: 'transparent',
            borderStyle: 'solid',
            borderLeftWidth: 50,
            borderRightWidth: 50,
            borderBottomWidth: 100,
            borderLeftColor: 'transparent',
            borderRightColor: 'transparent',
            borderBottomColor: '#0000',
            transform:[{ rotateZ: `${props.transformDegree}deg` }]
        }}
        >
        </View>
    )
}
*/

export default BusMarkerFactory;
