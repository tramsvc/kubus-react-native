import React, { Component } from 'react';
import { Alert, View, Text, Modal, TouchableOpacity } from 'react-native';
import {Picker} from '@react-native-community/picker';
import PropTypes from 'prop-types';
import { GetRouteInfo } from 'utils/data-fetcher';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faBusAlt, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { styles, titleBarStyles } from 'styles/map-setting';

export function BusAndRouteSelectorButton(props) {
    return (
        <TouchableOpacity style={styles.selectorButton} onPress={props.onPressHandler}>
            <FontAwesomeIcon icon={faBusAlt} color={'#fff'} size={18} />
        </TouchableOpacity>
    )
}

export function BusAndRouteSelectorModal(props) {
    return (
        <Modal
            animationType={'slide'}
            transparent={false}
            visible={props.showSelectorModal}
            onRequestClose={props.onModalToggle}>
            <View style={titleBarStyles.titleContainer}>
                <Text style={titleBarStyles.titleText}>{props.title}</Text>
                <TouchableOpacity style={titleBarStyles.closeButtonContainer} onPress={props.onModalToggle}>
                    <FontAwesomeIcon icon={faChevronDown} color={'#fff'} size={24} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                {props.children}
            </View>
        </Modal>
    )
}

export class RoutePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            routeData: null,
            isEnable: false
        }
    }

    componentDidMount() {
        this.FetchRouteData();
    }

    async FetchRouteData() {
        try {
            const FETCH_ROUTEDATA = await GetRouteInfo();
            this.setState({ routeData: FETCH_ROUTEDATA, isEnable: true });
        }
        catch (e) {
            this.setState({ routeData: -1 });
            Alert.alert("เกิดข้อผิดพลาดทางเครือข่าย", "ไม่สามารถดาวน์โหลดข้อมูลเส้นทางได้\nกรุณาตรวจสอบการเชื่อมต่อ หรืออาจเกิดปัญหาขัดข้องที่เซิร์ฟเวอร์");
        }
    }

    renderRoutePickerItem() {
        if (this.state.routeData === -1) {
            return <Picker.Item label="ไม่สามารถดาวน์โหลดข้อมูลเส้นทางได้" value={null} />
        }
        else if (this.state.routeData === null) {
            return <Picker.Item key={'r-1'} label="กำลังโหลดเส้นทาง..." value={null} />
        }
        else {
            return (
                this.state.routeData.map((route) => {
                    return <Picker.Item key={`r${route.id}`} label={`${route.name} - ${route.description}`} value={route.id} />
                })
            )
        }
    }
    render() {
        const { title, onRoutePickerChangeHandler, routeSelected, isEnable } = this.props;
        return (
            <View>
                <Text style={styles.titleText}>{title}</Text>
                <Picker
                    selectedValue={routeSelected}
                    style={styles.picker}
                    mode='dropdown'
                    enabled={isEnable}
                    onValueChange={(itemValue) => onRoutePickerChangeHandler(itemValue)}
                >
                    <Picker.Item key={'r0'} label={'กรุณาเลือกเส้นทางเดินรถ'} value={null} />
                    {this.renderRoutePickerItem()}
                </Picker>
            </View>
        )
    }
}

RoutePicker.propTypes = {
    title: PropTypes.string.isRequired
};

RoutePicker.defaultProps = {
    title: 'เส้นทางการเดินรถ',
};


export class BusPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEnable: false
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.busData !== prevProps.busData) {
            this.setState({ busData: this.props.busData });
        }
        if (this.props.routeSelected !== prevProps.routeSelected && this.props.routeSelected !== null) {
            this.setState({ routeSelected: this.props.routeSelected, isEnable: true });
        }
    }
    renderBusPickerItem() {
        const { routeSelected, busData } = this.props;
        let filterBusData = busData.filter(function (bus) {
            if (bus.is_active)
                return bus;
        });
        if (filterBusData !== null && routeSelected !== null) {
            return filterBusData.map(function (bus) {
                return <Picker.Item key={'b' + bus.bus_id} label={`ป้ายทะเบียน ${bus.plate}`} value={bus.bus_id} />
            })
        }
        else if (routeSelected !== null) {
            return <Picker.Item key={'b0'} label="เลือกรถประจำทางที่ต้องการ" value={null} />

        }
        else {
            return <Picker.Item key={'b0'} label="กรุณาเลือกเส้นทางการเดินรถก่อน" value={null} />
        }
    }

    handlePickerValueChange(itemValue) {
        if (itemValue !== null) {
            this.props.onBusPickerChangedHandler(itemValue);
        }
    }
    render() {
        const { selectedBus } = this.props;
        return (
            <View>
                <Text style={styles.titleText}>รถประจำทาง</Text>
                <Picker
                    selectedValue={selectedBus.bus_id}
                    style={styles.picker}
                    mode='dropdown'
                    onValueChange={(itemValue) => this.handlePickerValueChange(itemValue)}
                >
                    {this.renderBusPickerItem()}
                </Picker>
            </View>
        )
    }
}

