import React from 'react';
import { TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCrosshairs } from '@fortawesome/free-solid-svg-icons';
import { styles } from 'styles/map-setting';

export default function ZoomToTargetBusButton(props) {
    return (
        <TouchableOpacity style={styles.selectorButton} onPress={props.onPressHandler}>
            <FontAwesomeIcon icon={faCrosshairs} color={'#fff'} size={18} />
        </TouchableOpacity>
    )
}