import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

export default function BusInfoView(props) {
    return (
        <View style={styles.ContentContainer}>
            <View style={styles.IconContainer}>
                <FontAwesomeIcon icon={props.titleIcon} color='#fff' size={24} />
            </View>
            <View style={styles.InfoContainer}>
                <Text style={styles.HeaderText}>{props.title}</Text>
                <Text style={styles.DetailText} numberOfLines={1}>{props.info}</Text>
            </View>
        </View>
    )

}

const styles = StyleSheet.create({
    ContentContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
    },
    IconContainer: {
        justifyContent: 'center',
    },
    InfoContainer: {
        flex: 1
    },
    HeaderText: {
        color:'#fff',
        fontSize: 16,
        fontWeight: "bold",
        textAlign: "center",
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
    },
    DetailText: {
        color:'#fff',
        fontSize: 14,
        textAlign: "center",
        marginRight: 5,
    }
})