import React from 'react';
import { View, StyleSheet } from 'react-native';
import BusInfoView from 'map-components/map-bus-info/map-busInfoView';
import SlideUpPanel from 'ui-components/slide-up-panel';
import { faClock, faTachometerAlt, faRoute, faBus, faRoad } from '@fortawesome/free-solid-svg-icons';

const HEIGHT = 230;
export default function BusInfo(props) {
    if (props.targetBusData !== null)
        return (
            <SlideUpPanel
                reference={props.panelReference}
                height={HEIGHT}
                draggableRange={{ top: HEIGHT, bottom: 20 }}
                animatedValue={props.animatedValue}
                showBackdrop={false}
                icon={faBus}
                title={`${props.targetBusData.route_name} `}
                subtitle={props.targetBusData.description}
            >
                <View style={mapInfoStyles.MapInfoContainer}>
                    <View style={mapInfoStyles.MapInfoRow}>
                        <BusInfoView title='ป้ายทะเบียน' info={props.targetBusData.plate} titleIcon={faBus} />
                        <BusInfoView title='เวลารอรถ' info={props.targetBusData.waitTime} titleIcon={faClock} />
                    </View>
                    <View style={mapInfoStyles.MapInfoRow}>
                        <BusInfoView title='ความเร็ว' info={`${Math.round((props.targetBusData.speed + Number.EPSILON) * 100) / 100} km/hr`} titleIcon={faTachometerAlt} />
                        <BusInfoView title='ระยะห่าง' info={props.targetBusData.displayDistance} titleIcon={faRoute} />
                    </View>
                </View>
            </SlideUpPanel>
        )
        else
        return(
            <SlideUpPanel
            reference={props.panelReference}
            height={210}
            draggableRange={{ top: HEIGHT, bottom: 20 }}
            animatedValue={props.animatedValue}
            showBackdrop={false}
            icon={faBus}
            title={'ข้อมูลรถประจำทาง'}
        >
            <View style={mapInfoStyles.MapInfoContainer}>
                <View style={mapInfoStyles.MapInfoRow}>
                    <BusInfoView title='เส้นทางเดินรถ' info={'-'} titleIcon={faRoad} />
                    <BusInfoView title='เวลารอรถ' info={'-'} titleIcon={faClock} />
                </View>
                <View style={mapInfoStyles.MapInfoRow}>
                    <BusInfoView title='ความเร็ว' info={'-'} titleIcon={faTachometerAlt} />
                    <BusInfoView title='ระยะห่าง' info={'-'} titleIcon={faRoute} />
                </View>
            </View>
        </SlideUpPanel>
        )
}

const mapInfoStyles = StyleSheet.create({
    MapInfoContainer: {
        flex: 1,
        flexDirection: 'column',
        flexWrap: 'wrap',
        marginBottom: 5,
    },
    MapInfoRow: {
        flex: 1,
        flexDirection: 'row'
    },
});