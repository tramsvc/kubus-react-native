import React, { PureComponent } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SlideUpPanel from 'ui-components/slide-up-panel';
import { faParking } from '@fortawesome/free-solid-svg-icons';
import { infoBarStyles } from 'styles/station-info';

const HEIGHT = 260;

class StationInfo extends PureComponent {
    constructor(props) {
        super(props);
        this._handleOnRowPress = this._handleOnRowPress.bind(this);
    }
    _handleOnRowPress(routeID) {
        if (this.props.onPress)
            this.props.onPress(routeID);
        else
            return;
    }
    render() {
        return (
            <SlideUpPanel reference={this.props.panelRef} animatedValue={this.props.animatedValue} height={HEIGHT} draggableRange={{ top: HEIGHT, bottom: 0 }} showBackdrop={false} icon={faParking} title={this.props.stationName} subtitle={null}>
                <ScrollView>
                    {
                        this.props.routesData.map((route) => {
                            return <InfoBar key={route.route_id} routeID={route.route_id} title={route.route_name} detail={route.route_description} onRowPress={this._handleOnRowPress} />
                        })
                    }
                </ScrollView>
            </SlideUpPanel>
        )
    }
}

class InfoBar extends PureComponent {
    constructor(props) {
        super(props);
    }
    _handleOnPress = () => {
        if (this.props.onRowPress)
            this.props.onRowPress(this.props.routeID);
        else
            return;
    }
    render() {
        return (
            <TouchableOpacity onPress={this._handleOnPress} style={infoBarStyles.box}>
                <Text style={infoBarStyles.titleText}>{this.props.title}</Text>
                <Text style={infoBarStyles.detailText}>{this.props.detail}</Text>
                <View style={infoBarStyles.horizontalLine} />
            </TouchableOpacity>
        )
    }
}

export default StationInfo;