import React, { PureComponent } from 'react';
import { Dimensions, Platform } from 'react-native';
import { Marker, Polyline, AnimatedRegion } from 'react-native-maps';
import BusMarkerFactory from 'map-components/bus-marker-factory';
//ICON
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faStreetView, faParking } from '@fortawesome/free-solid-svg-icons';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const DURATION = 1500;
const STOP_TRACKVIEW_DURATION = 1000;

class BusMarker extends PureComponent {
    constructor(props) {
        super(props);
        this._intervalID = null;
        this.state = {
            busData: this.props.busData,
            tracksViewChanges: true
        };
        this._markerColor = this._markerColor.bind(this);
        this._stopTrackViewChanges = this._stopTrackViewChanges.bind(this);
    }
    componentDidMount() {
        this._intervalID = setTimeout(this._stopTrackViewChanges, STOP_TRACKVIEW_DURATION);
        this._showTargetCallout();
    }
    componentWillUnmount() {
        if (this._intervalID !== null && typeof this._intervalID !== undefined) {
            clearTimeout(this._intervalID);
        }
    }
    componentDidUpdate(prevProps) {
        if (prevProps.busData !== this.props.busData) {
            this.setState({ tracksViewChanges: true });
            const coords = { latitude: this.props.busData.latitude, longitude: this.props.busData.longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA };
            const oldCoordinate = new AnimatedRegion({ latitude: prevProps.busData.latitude, longitude: prevProps.busData.longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA });
            const newCoordinate = new AnimatedRegion({ coords });
            if (Platform.OS === 'android') {
                this._marker.animateMarkerToCoordinate({
                    latitude: this.props.busData.latitude,
                    longitude: this.props.busData.longitude
                }, DURATION);
            }
            else {
                oldCoordinate.timing({ newCoordinate, DURATION }).start();
            }
            this.setState({ busData: this.props.busData });
            this._intervalID = setTimeout(this._stopTrackViewChanges, STOP_TRACKVIEW_DURATION);

        }
    }

    _showTargetCallout() {
        if (this.props.isTargetBus) {
            this._marker.showCallout();
        }
    }
    _stopTrackViewChanges() {
        this.setState({ tracksViewChanges: false });
    }

    _markerColor() {
        let markerColor = null;
        this.props.busData.is_active ? markerColor = this.props.busData.color : markerColor = '#C0C0C0';
        return markerColor;
    }
    render() {
        return (
            <Marker.Animated
                coordinate={{ latitude: this.state.busData.latitude, longitude: this.state.busData.longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA }}
                ref={marker => this._marker = marker}
                title={this.props.busData.route_name}
                description={this.props.busData.plate}
                anchor={this.props.anchor}
                onPress={this.props.onPress}
                style={{ zIndex: 1 }}
                tracksViewChanges={this.state.tracksViewChanges}
            >
                <BusMarkerFactory courseDeg={this.props.busData.course} color={this._markerColor()} />
            </Marker.Animated>
        )
    }
}

class StationMarker extends PureComponent {
    constructor(props) {
        super(props);
        this._intervalID = null;
        this.state = { stationData: this.props.data, isNearest: this.props.isNearestStation, tracksViewChanges: true };
        this._stopTrackViewChanges = this._stopTrackViewChanges.bind(this);
    }
    componentDidMount() {
        this._intervalID = setTimeout(this._stopTrackViewChanges, STOP_TRACKVIEW_DURATION);
    }
    componentWillUnmount() {
        if (this._intervalID !== null && typeof this._intervalID !== undefined) {
            clearTimeout(this._intervalID);
        }
    }
    componentDidUpdate(prevProps) {
        if (prevProps.data !== this.props.data || prevProps.isNearestStation !== this.props.isNearestStation) {
            this.setState({ stationData: this.props.data, isNearest: this.props.isNearestStation, tracksViewChanges: true });
            this._intervalID = setTimeout(this._stopTrackViewChanges, STOP_TRACKVIEW_DURATION);
        }
    }
    _stopTrackViewChanges() {
        this.setState({ tracksViewChanges: false });
    }
    render() {
        return (
          <Marker
              coordinate={{ latitude: parseFloat(this.state.stationData.latitude), longitude: parseFloat(this.state.stationData.longitude) }}
              title={this.state.stationData.station_name}
              tracksViewChanges={this.state.tracksViewChanges}
              onPress={this.props.onPress}
          >
              <FontAwesomeIcon icon={faParking} size={20} color={this.props.isNearestStation ? 'red' : 'black'} />
          </Marker>
        )
    }
}
function NearestBusMarker(props) {
    if (props.data) {
        return (
            <Marker
                anchor={props.anchor}
                coordinate={{ latitude: parseFloat(props.data.latitude), longitude: parseFloat(props.data.longitude) }}
                key={props.data.bus_id}
                icon={require('busmarker/targetbus.png')}
                pinColor='yellow'
                tracksViewChanges={false}
            />
        )
    }
    else
        return null;
}
function UserMarker(props) {
    if (props.coordinate) {
        return (
            <Marker
                title={`ตำแหน่งผู้ใช้งาน`}
                coordinate={{ latitude: parseFloat(props.coordinate.latitude), longitude: parseFloat(props.coordinate.longitude) }}
                tracksViewChanges={false}
                style={{ zIndex: 2 }}
            >
                <FontAwesomeIcon icon={faStreetView} color='blue' size={32} />
            </Marker>
        );
    }
    else
        return null;
}
function RoutePolyline(props) {
    return (
        <Polyline
            coordinates={props.data}
            strokeWidth={2}
            geodesic={true}
            strokeColor={props.color}
        />
    );
}

function TargetPolyline(props) {
    return (
        <Polyline
            coordinates={props.data}
            geodesic={true}
            strokeWidth={4}
            strokeColor={'#ff0000'}
            style={{zIndex: 5}}
        />
    );
}

export { BusMarker, StationMarker, NearestBusMarker, UserMarker, RoutePolyline, TargetPolyline };
