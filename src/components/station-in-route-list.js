import React, { Component } from 'react';
import { Dimensions, View, Text, FlatList, SafeAreaView } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import PropTypes from 'prop-types';
import { styles, colorContainerStyle } from 'styles/station-in-route-list';
import { RoutePicker } from 'map-components/bus-route-selector';
import { GetWaypointInRoute } from 'utils/data-fetcher';
import NetNotifier from '../utils/net-notifier';
import { TouchableOpacity } from 'react-native-gesture-handler';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LAT_DELTA = 0.0122;
const LON_DELTA = LAT_DELTA * ASPECT_RATIO;

export default class StationInRouteList extends Component {
    static navigationOptions = {
        title: 'ตรวจสอบสถานีในเส้นทาง',
    };
    constructor(props) {
        super(props);
        this.state = {
            route: null,
            stationData: null
        };
    }
    _renderSeparator() {
        return (
            <View style={styles.separator} />
        );
    }
    _routePickerValueChanged = async (value) => {
        const DATA = await GetWaypointInRoute(value);
        this.setState({ route: value, stationData: DATA });
    }
    _renderEmptyListText() {
        return <Text style={styles.emptyListText}>ไม่มีผลลัพธ์</Text>;
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }} >
                <NetNotifier />
                <View style={styles.container}>
                    <RoutePicker onRoutePickerChangeHandler={this._routePickerValueChanged} routeSelected={this.state.route} isEnable={true} />
                    <View style={styles.flatList}>
                        <FlatList
                            ItemSeparatorComponent={this._renderSeparator}
                            ListEmptyComponent={this._renderEmptyListText}
                            data={this.state.stationData}
                            renderItem={({ item }) =>
                                <Item
                                    id={item.step}
                                    color={item.color}
                                    coordinate={{ latitude: item.latitude, longitude: item.longitude, latitudeDelta: LAT_DELTA, longitudeDelta: LON_DELTA }}
                                    title={item.station_name}
                                />
                            }
                            keyExtractor={item => item.step.toString()}
                        />
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMiniMapShow: false
        }
        this._handleOnPress = this._handleOnPress.bind(this);
    }
    _handleOnPress() {
        this.setState({ isMiniMapShow: !this.state.isMiniMapShow })
    }
    _renderMiniMap() {
        const { id, title, coordinate } = this.props;
        const { isMiniMapShow } = this.state;
        if (isMiniMapShow)
            return (
                <MapView style={{ height: 300 }}
                    initialRegion={coordinate}
                    provider={PROVIDER_GOOGLE}
                    showsCompass={false}
                    showsUserLocation={true}
                >
                    <Marker
                        key={id}
                        title={title}
                        description={`${coordinate.latitude},${coordinate.longitude}`}
                        icon={require('bus-stop-marker/bus-stop.png')}
                        coordinate={coordinate}
                    />
                </MapView>
            )
        else
            return null;

    }
    render() {
        const { color, title } = this.props;
        return (
            <TouchableOpacity onPress={this._handleOnPress}>
                <View style={styles.item}>
                    <View style={colorContainerStyle(color)} />
                    <View style={styles.textContainer}>
                        <Text>{title}</Text>
                    </View>
                </View>
                <View>
                    {this._renderMiniMap()}
                </View>
            </TouchableOpacity>
        )
    }
}

Item.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
}