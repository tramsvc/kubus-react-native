import React, { PureComponent } from 'react';
import { Alert, Text, View, Image, ImageBackground } from 'react-native';
import Loader from 'ui-components/loader';
import NetNotifier from 'utils/net-notifier';
import { styles } from 'styles/home-screen';
import config from 'config/config';
import Ripple from 'react-native-material-ripple';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faQrcode, faLocationArrow, faBus, faParking/*, faGlobeAsia*/ } from '@fortawesome/free-solid-svg-icons';
import RouteSelectorModal from './map-setting-components/route-selector-modal';

import Geolocation from 'react-native-geolocation-service';
import { requestLocationPermission } from 'utils/permission';
import { FindClosestCoordinate, FilterMultipleRouteAndStepInStation } from 'utils/new-routing-function';
import { GetStationLocation, GetRouteInStation } from 'utils/data-fetcher';
import { MapSelectRouteRadioOption } from 'utils/radio-option-mapper';

const getPositionTimeout = config.gps.getPositionTimeout;
const maximumAgeTime = config.gps.getPositionMaxAge;
const useHighAccuracy = config.gps.useHighAccuracy;

export default class HomeScreen extends PureComponent {
  constructor(props) {
    super(props);
    this._onQRCodeNavPressed = this._onQRCodeNavPressed.bind(this);
    this._onCheckStationPressed = this._onCheckStationPressed.bind(this);
    this._onFindByGPSPressed = this._onFindByGPSPressed.bind(this);
    this._setSelectorModalVisible = this._setSelectorModalVisible.bind(this);
    this._setActivityLoading = this._setActivityLoading.bind(this);
    //this._onSimulationButtonPressed = this._onSimulationButtonPressed.bind(this);
    this.state = {
      isLoading: false,
      mapMode: null,
      radioOptions: [],
      routeSelectModalVisible: false,
      coordinateSource: 'gps',
      nearestStation: null,
      simFlag: false,
      sliderButtonDisabled: true,
      userCoordinate: null
    }
  }

  _setSelectorModalVisible() {
    this.setState({ routeSelectModalVisible: !this.state.routeSelectModalVisible });
  }
  _setActivityLoading(status) {
    this.setState({ isLoading: status })
  }
  GenerateOptions = async (userCoordinate) => {
    const STATION_DATA = await GetStationLocation();
    const STATION_NEAR_USER = FindClosestCoordinate(STATION_DATA, userCoordinate);
    const FilterMultipleStations = await FilterMultipleRouteAndStepInStation(STATION_NEAR_USER, STATION_DATA);
    if (FilterMultipleStations.length > 1) {
      const routeData = await GetRouteInStation(FilterMultipleStations[0].station_id);
      const MappedOptions = MapSelectRouteRadioOption(routeData);
      this.setState({ radioOptions: MappedOptions, nearestStation: FilterMultipleStations[0] });
      this._setSelectorModalVisible();
    }
    else if (FilterMultipleStations.constructor !== Array) {
      this.props.loaderHandler(false);
      Alert.alert('เกิดข้อผิดพลาด', 'การค้นหาตำแหน่งจาก GPS ผิดพลาด', [{ text: 'OK' }]);
    }
    else {
      this._setActivityLoading(false);
      this.NavigationHandler(this.state.userCoordinate, this.state.coordinateSource, FilterMultipleStations[0].route_id, FilterMultipleStations[0]);
    }
  }

  NavigationHandler = (userCoordinate, coordinateSrc, routeSelected, nearbyStation) => {
    /*
    let navName = simulationFlag ? 'MarkerSimulation' : 'Map'
    if (routeSelected === -1)
      this.props.navigation.navigate('AllBusMap', { receivedCoordinate: userCoordinate, resultFrom: coordinateSrc, routeSelected: routeSelected });
    else
    */
    this.props.navigation.navigate('Map', { receivedCoordinate: userCoordinate, resultFrom: coordinateSrc, routeSelected: routeSelected, nearestStation: nearbyStation });
  }
  //Simulation Mode
  /*
async _onSimulationButtonPressed() {
  const isGranted = await requestLocationPermission();
  this._setActivityLoading(true);
  if (isGranted) {
    await Geolocation.getCurrentPosition(async (position) => await this.SimulationGeolocationSuccessCallback(position),
      (error) => {
        this._setActivityLoading(false);
        Alert.alert('Error', error.message, [{ text: 'OK' }])
      },
      { enableHighAccuracy: true, timeout: 8000, maximumAge: 8000 }
    );
  }
  else {
    this._setActivityLoading(false);
    Alert.alert('เกิดข้อผิดพลาด', 'ไม่ได้รับสิทธิ์ในการเข้าถึงตำแหน่ง\nลองใหม่อีกครั้ง จากนั้นทำการอนุญาตให้เข้าถึงตำแหน่ง', [{ text: 'OK' }])
  }
}
//Simulation Mode OnPress event

async SimulationGeolocationSuccessCallback(position) {
  try {
    let userCoord = {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    }
    this.setState({ userCoordinate: userCoord, simFlag: true });
    await this.GenerateOptions(userCoord);

  }
  catch (error) {
    this._setActivityLoading(false);
    Alert.alert('เกิดข้อผิดพลาด', error.message, [{ text: 'OK', onPress: () => this._setActivityLoading(false) }]);
  }
}
*/

  async _onFindByGPSPressed() {
    const isGranted = await requestLocationPermission();
    this._setActivityLoading(true);
    if (isGranted) {
      await Geolocation.getCurrentPosition(async (position) => await this.GeolocationSuccessCallback(position),
        (error) => {
          this._setActivityLoading(false);
          Alert.alert('Error', error.message, [{ text: 'OK' }]);
        },
        { enableHighAccuracy: useHighAccuracy, timeout: getPositionTimeout, maximumAge: maximumAgeTime }
      );
    }
    else {
      this._setActivityLoading(false);
      Alert.alert('เกิดข้อผิดพลาด', 'ไม่ได้รับสิทธิ์ในการเข้าถึงตำแหน่ง\nลองใหม่อีกครั้ง จากนั้นทำการอนุญาตให้เข้าถึงตำแหน่ง', [{ text: 'OK' }]);
    }
  }
  async GeolocationSuccessCallback(position) {
    try {
      let userCoord = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      }
      this.setState({ userCoordinate: userCoord, simFlag: false });
      await this.GenerateOptions(userCoord);
    }
    catch (error) {
      this._setActivityLoading(false);
      Alert.alert('เกิดข้อผิดพลาด', error.message, [{ text: 'OK' }])
    }
  }

  _onQRCodeNavPressed() {
    this.props.navigation.navigate('QRScanner');
  }
  _onAllBusMapPressed = () => {
    this.props.navigation.navigate('AllBusMap');
  }
  _onGPSAccuracyTest = () => {
    this.props.navigation.navigate('Test');
  }
  _onCheckStationPressed() {
    this.props.navigation.navigate('BusRouteImage');
  }

  render() {
    return (
      <ImageBackground source={require('../../assets/menu_bg/bg.png')} style={styles.image_bg_container}>
        <View style={styles.notifier_container}>
          <NetNotifier />
        </View>
        <View style={styles.container}>
          <Image source={require('../../assets/menu_bg/logo/Logo_KU_thai_color.png')} style={styles.logo} resizeMode='contain' />
          <Text style={styles.title}>แอพค้นหารถไบโอดีเซล</Text>
          <Text style={styles.subtitle}>มหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตกำแพงแสน</Text>
          <MenuButton function={this._onQRCodeNavPressed} icon={faQrcode} buttonText={'ค้นหาตำแหน่งโดยการสแกน QRCode'} />
          <MenuButton function={this._onFindByGPSPressed} icon={faLocationArrow} buttonText={'ค้นหาตำแหน่งโดยใช้ GPS'} />
          <MenuButton function={this._onAllBusMapPressed} icon={faBus} buttonText={'ดูตำแหน่งรถทั้งหมด'} />
          <MenuButton function={this._onCheckStationPressed} icon={faParking} buttonText={'ตรวจสอบเส้นทางเดินรถ'} />
          {
            /*
          <MenuButton function={this._onGPSAccuracyTest} icon={faGlobeAsia} buttonText={'ทดสอบความแม่นยำ GPS'} />
          <MenuButton function={this._onSimulationButtonPressed} icon={faRobot} buttonText={'โหมดจำลองการอัปเดตรถ (GPS)'} />
            */
          }
          {
            this.state.isLoading && <Loader loading={this.state.isLoading} size="large" color="#FFF" />
          }
          {
            this.state.routeSelectModalVisible &&
            <RouteSelectorModal
              coordinateSource={this.state.coordinateSource}
              nearestStation={this.state.nearestStation}
              modalVisible={this.state.routeSelectModalVisible}
              modalVisibilityHandler={this._setSelectorModalVisible}
              radioOptions={this.state.radioOptions}
              navHandler={this.NavigationHandler}
              loaderHandler={this._setActivityLoading}
              simulationFlag={this.state.simFlag}
              userCoordinate={this.state.userCoordinate}
            />
          }
        </View>
      </ImageBackground>
    );
  }
}

const MenuButton = (props) => {
  return (
    <Ripple onPress={props.function}>
      <View style={styles.menuBorder}>
        <FontAwesomeIcon style={styles.menuIcon} icon={props.icon} />
        <View style={styles.menuTextContainer}>
          <Text style={styles.menuText}>{props.buttonText}</Text>
        </View>
      </View>
    </Ripple>
  );
}