import React from 'react';
import { Dimensions, Image, View } from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';

const { width, height } = Dimensions.get('window')

const BusRouteImage = () => {

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor:'#000' }}>
            <ImageZoom
                cropWidth={width}
                cropHeight={height}
                imageWidth={width}
                imageHeight={height}
            >
                <Image
                style={{width: width, height:height, resizeMode: 'contain'}}
                    source={require('assets/bus_route/kps-bus-routes.jpg')}
                />
            </ImageZoom>
        </View>
    )
}
export default BusRouteImage;