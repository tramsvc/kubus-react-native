import React, { Component } from 'react';
import { Dimensions, Text, View, Button, Modal } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { UserMarker, RoutePolyline, StationMarker } from 'map-components/markerView';
import { modalStyles } from 'styles/home-screen';
//Radio
import { RadioGroup } from 'react-native-btr';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faBus } from '@fortawesome/free-solid-svg-icons';
import config from 'config/config';
//API Service
import { GetStationsDataInRoute, GetAllStationsData } from 'utils/data-fetcher';
//Polyline Services
import PolylineData from 'assets/polylinedata/polyline_data.json';
import { DecodePolyline } from 'utils/decode-google-direction';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = config.map.KU_Lat;
const LONGITUDE = config.map.KU_Lon;
const LATITUDE_DELTA = 0.0122;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class RouteSelectorModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            stationData: null,
            radioOptions: null,
            selectedValue: null,
            positiveButtonDisabled: true
        };
    }
    OnRadioButtonPressed = async (option) => {
        const selectedValue = option.find(e => e.checked === true).value;
        let station_data = null;
        if (selectedValue !== -1)
            station_data = await GetStationsDataInRoute(selectedValue);
        else
            station_data = await GetAllStationsData();
        this.setState({ radioOptions: option, positiveButtonDisabled: false, selectedValue: selectedValue, stationData: station_data });
    }

    OnPositiveButtonPressed = () => {
        const { userCoordinate, coordinateSource, nearestStation } = this.props;
        const selectedValue = this.state.radioOptions.find(e => e.checked === true).value;
        if (this.props.coordinateSource === 'gps')
            this.props.loaderHandler(false);
        this.props.modalVisibilityHandler();
        this.props.navHandler(userCoordinate, coordinateSource, selectedValue, nearestStation);
        /*
        if (coordinateSource === 'qrcode') {
            this.props.navHandler(userCoordinate, coordinateSource, selectedValue);
        }
        else {
            this.props.navHandler(userCoordinate, coordinateSource, selectedValue, simulationFlag);
        }
        */
    }

    OnCancelButtonPressed = () => {
        if (this.props.coordinateSource === 'gps')
            this.props.loaderHandler(false);
        else
            this.props.onCancelPress();
        this.props.modalVisibilityHandler();
    }

    HandleLoader = (value) => {
        this.props.loaderHandler(value);
    }

    _HandleOnRequestClose = () => {
        if (this.props.coordinateSource === 'gps')
            this.props.loaderHandler(false);
        this.props.modalVisibilityHandler();
    }
    _renderNearestStationText() {
        if (this.props.nearestStation) {
            let text = null;
            switch (this.props.coordinateSource) {
                case 'gps':
                    text = 'สถานีที่อยู่ใกล้คุณ: ';
                    break;
                case 'qrcode':
                    text = 'สถานีที่คุณสแกน: ';
                    break;
                default:
                    break;
            }
            return (
                <View style={modalStyles.nearestStationContainer}>
                    <Text style={modalStyles.nearestStationText}>{text}{this.props.nearestStation.station_name}</Text>
                </View>
            );

        }
        else
            return null;
    }
    _renderMapView() {
        return (
            <MapView
                initialRegion={this.state.region}
                provider={PROVIDER_GOOGLE}
                showsCompass={false}
                onMapReady={this._onMapReadyInit}
                style={{ width: '100%', height: '100%' }}
            >
                {this._renderPolyline()}
                {this._renderStationMarkers()}
                <UserMarker coordinate={this.props.userCoordinate} />
            </MapView>
        )
    }
    _onMapReadyInit = async() =>{
        const station_data = await GetAllStationsData();
        this.setState({ stationData: station_data });
    }
    _renderStationMarkers() {
        const { stationData } = this.state;
        if (this.state.stationData !== null) {
            return stationData.map((marker) => {
                let isNearest = this.props.nearestStation.station_id === marker.station_id ? true : false;
                return <StationMarker
                    data={marker}
                    key={marker.station_id}
                    isNearestStation={isNearest}
                />
            });
        }
        else return null;
    }
    _renderPolyline() {
        if (this.state.selectedValue !== null && this.state.selectedValue !== -1) {
            const POLYLINE_DATA = DecodePolyline(PolylineData, this.state.selectedValue);
            return <RoutePolyline data={POLYLINE_DATA} color={POLYLINE_DATA.color} />
        }
        else {
            const POLYLINE_DATA = DecodePolyline(PolylineData);
            return POLYLINE_DATA.map((e) => {
                return <RoutePolyline data={e.coords} color={e.color} />
            });
        }
    }
    _renderUserMarker() {
        if (this.props.userCoordinate !== null) {
            return <UserMarker coordinate={userCoord} />
        }
        else {
            return null;
        }
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={this.OnCancelButtonPressed}
            >
                <View style={modalStyles.panelContainer}>
                    <View style={modalStyles.titleContainer}>
                        <FontAwesomeIcon style={{ marginRight: 10 }} color={'#fff'} icon={faBus} size={18} />
                        <Text style={modalStyles.titleBarText}>เลือกสายการเดินรถที่ต้องการ</Text>
                    </View>
                    <View style={modalStyles.radioGroup}>
                        {this._renderNearestStationText()}
                        <RadioSelector handleRadioPress={this.OnRadioButtonPressed} radioOptions={this.props.radioOptions} />
                    </View>
                    <View style={{ flex: 1 }}>
                        {this._renderMapView()}
                    </View>
                    <View style={modalStyles.buttonRow}>
                        <View style={modalStyles.button}>
                            <Button color={'gray'} title="ยกเลิก" onPress={this.OnCancelButtonPressed} />
                        </View>
                        <View style={modalStyles.button}>
                            <Button title="ตกลง" disabled={this.state.positiveButtonDisabled} onPress={this.OnPositiveButtonPressed} />
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

export const RadioSelector = ({ radioOptions, handleRadioPress }) => {
    HandleRadioPress = (option) => {
        handleRadioPress(option);
    }
    return (
        <RadioGroup labelStyle={{ fontSize: 16, flex: 1, flexWrap: 'wrap' }} radioButtons={radioOptions} onPress={radioOptions => HandleRadioPress(radioOptions)} />
    )
}