import React, { Component } from 'react';
import { View, Text, Modal, Switch, TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCog, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { styles, titleBarStyles } from 'styles/map-setting';

export function MapSettingButton(props) {
    return (
        <TouchableOpacity style={styles.mapSettingButton} onPress={props.onPressHandler}>
            <FontAwesomeIcon icon={faCog} color={'#fff'} size={18} />
        </TouchableOpacity>
    )
}

export function SettingModal(props) {
    return (
        <Modal
            animationType={'slide'}
            transparent={false}
            visible={props.showSettingModal}
            onRequestClose={props.onModalToggle}>
            <View style={titleBarStyles.titleContainer}>
                <Text style={titleBarStyles.titleText}>{props.title}</Text>
                <TouchableOpacity style={titleBarStyles.closeButtonContainer} onPress={props.onModalToggle}>
                    <FontAwesomeIcon icon={faChevronDown} color={'#fff'} size={24} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                {props.children}
            </View>
        </Modal>
    )
}

export class DisplayMapSetting extends Component {
    constructor(props) {
        super(props);
    }
    handleToggleSetting(value, key) {
        const { options, onSettingChangeHandler } = this.props;
        let changedOptions = options.map(option => {
            if (option.name === key) {
                option.value = value;
            }
            return option;
        });
        onSettingChangeHandler(changedOptions);
    }

    render() {
        const { options } = this.props;
        return (
            <View>
                <Text style={styles.titleText}>การแสดงผลของหมุดปักบนแผนที่</Text>
                {
                    options.map((activity, index) => {
                        const key = activity.name;
                        return (
                            <View style={styles.displaySwitch}>
                                <Text key={'switchtext' + index}>{key}</Text>
                                <Switch key={'switch' + index} value={activity.value} onValueChange={(value) => this.handleToggleSetting(value, key)} />
                            </View>
                        );
                    })
                }
            </View>
        )
    }
}
