'use strict';
import React, { PureComponent } from 'react';
import { Alert, View, Text, TouchableWithoutFeedback } from 'react-native';
import BarcodeMask from 'react-native-barcode-mask';
import { RNCamera } from 'react-native-camera';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { CheckCameraPermissionGranted, RequestCameraPermission } from 'utils/permission';
import { faCamera } from '@fortawesome/free-solid-svg-icons';
import config from 'config/config';

import RouteSelectorModal from './map-setting-components/route-selector-modal';
import { GetRouteAndStationDataForQRPage } from 'utils/data-fetcher';
import { /*QRMapSelectRouteRadioOption,*/ MapSelectRouteRadioOption } from 'utils/radio-option-mapper';

import TryParseJSON from 'utils/qrcode-utils';
import { styles } from 'styles/qr-scanner';

let didFocusSubscription = null;
let didBlurSubscription = null;
const resumeTimeAfterPause = config.qr.resumeTimeAfterPause;

export default class QRScannerScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.intervalID = null;
    this.onCancelPress = this.onCancelPress.bind(this);
    this.RequestPermission = this.RequestPermission.bind(this);
    this.state = {
      barcodescanPause: false,
      focusedScreen: true,
      cameraPermission: false,
      cameraText: 'กำลังค้นหา QRCode',
      coordinateSrc: 'qrcode',
      radioOptions: [],
      routeSelect: null,
      routeSelectModalVisible: false,
      stationData: null,
      sliderButtonDisabled: true,
      userCoordinate: null
    }
  }

  async componentDidMount() {
    const { navigation } = this.props;
    const permissionStatus = await CheckCameraPermissionGranted();
    this.setState({ cameraPermission: permissionStatus });
    if (!permissionStatus) {
      this.RequestPermission();
    }
    didFocusSubscription = navigation.addListener('focus', () => {
      setTimeout(() => {
        this.setState({ focusedScreen: true, barcodescanPause: false, cameraText: 'กำลังค้นหา QRCode' })
      }, 1000);
    });
    didBlurSubscription = navigation.addListener('blur', () => {
      this.setState({ focusedScreen: false, barcodescanPause: true, cameraText: 'กล้องถูกหยุด' })
    });
  }

  componentWillUnmount() {
    if (this.intervalID !== null && typeof this.intervalID !== undefined)
      clearTimeout(this.intervalID);
    didFocusSubscription();
    didBlurSubscription();
  }

  onCancelPress() {
    this._QRCodeScanResume();
  }

  async RequestPermission() {
    let status = await RequestCameraPermission();
    this.setState({ cameraPermission: status });
  }

  setModalVisible = () => {
    this.setState({ routeSelectModalVisible: !this.state.routeSelectModalVisible });
  }
  _RouteOptionMapper(response) {
    //const mappedOptions = QRMapSelectRouteRadioOption(response);
    const mappedOptions = MapSelectRouteRadioOption(response);
    this.setState({ radioOptions: mappedOptions });
    this.setModalVisible();
  }
  _QRCodeScanResume() {
    this.setState({ cameraText: "QR Code Paused." });
    this.intervalID = setTimeout(() => {
      this.setState({ barcodescanPause: false, cameraText: "กำลังค้นหา QR Code" });
    }, resumeTimeAfterPause);
  }
  _QRCodeScanPause() {
    this.setState({ barcodescanPause: true, cameraText: 'พบ QRCode' });
  }
  _handleNavigation = (userCoordinate, coordinateSrc, routeSelected, nearbyStation) => {
    this.props.navigation.navigate('Map', { receivedCoordinate: userCoordinate, resultFrom: coordinateSrc, routeSelected: routeSelected, nearestStation: nearbyStation });
  }

  onBarcodeRead = async (e) => {
    let result;
    try {
      if (!this.state.barcodescanPause) {
        this._QRCodeScanPause();
        result = TryParseJSON(e.data);
        if (result) {
          const response = await GetRouteAndStationDataForQRPage(result.id);
          this.setState({ stationData: response, userCoordinate: { latitude: response[0].latitude, longitude: response[0].longitude } });
          if (response.length > 1) {
            this._RouteOptionMapper(response);
          }
          else {
            this._handleNavigation(this.state.userCoordinate, this.state.coordinateSrc, this.state.stationData[0].route_id, this.state.stationData[0]);
          }
        }
        else {
          Alert.alert('คำเตือน', 'ไม่ใช่รูปแบบ QRCode ที่รองรับ\n กรุณาสแกน QRCode ที่ป้ายรถประจำทางไบโอดีเซล.', [{ text: 'OK', onPress: () => this._QRCodeScanResume() }], {
            onDismiss: () => {
              this._QRCodeScanResume();
            }
          });
        }
      }
    }
    catch (e) {
      Alert.alert('เกิดข้อผิดพลาด',
        `มีข้อผิดพลาดที่คาดไม่ถึงเกิดขึ้น\nรายละเอียด: ${e.message}`,
        [{ text: 'OK', onPress: () => this._QRCodeScanResume() }], {
        onDismiss: () => {
          this._QRCodeScanResume();
        }
      });
    }
  }

  renderCameraView() {
    return (
      <View style={styles.container}>
        <RNCamera
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
          onBarCodeRead={this.props.navigation.isFocused() ? this.onBarcodeRead : null}
        />
        <BarcodeMask showAnimatedLine={false} />
        <View style={{ flex: 0, height: 50, backgroundColor: 'white', justifyContent: 'center' }}>
          <Text style={{ textAlign: 'center' }}>{this.state.cameraText}</Text>
        </View>
        {
          this.state.routeSelectModalVisible &&
          <RouteSelectorModal
            coordinateSource={this.state.coordinateSrc}
            modalVisible={this.state.routeSelectModalVisible}
            modalVisibilityHandler={this.setModalVisible}
            nearestStation={this.state.stationData[0]}
            navHandler={this._handleNavigation}
            onCancelPress={this.onCancelPress}
            radioOptions={this.state.radioOptions}
            userCoordinate={this.state.userCoordinate}
          />
        }
      </View>
    )
  }

  renderCameraNotGrantedView() {
    return (
      <TouchableWithoutFeedback style={{ margin: 10 }} onPress={this.RequestPermission}>
        <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
          <FontAwesomeIcon style={{ alignSelf: 'center', marginBottom: 5 }} icon={faCamera} size={36} />
          <Text style={{ textAlign: 'center' }}>ไม่ได้การอนุญาตสิทธิ์ในการใช้กล้องถ่ายรูป{'\n'}แตะบริเวณนี้อีกครั้ง เพื่อเปิดการขออนุญาตสิทธิ์</Text>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  renderEmptyView() {
    return (
      <View style={{ flex: 1, margin: 10, justifyContent: 'center' }}>
        <FontAwesomeIcon style={{ alignSelf: 'center', marginBottom: 5 }} icon={faCamera} size={36} />
      </View>
    )
  }

  render() {
    const { focusedScreen, cameraPermission } = this.state;
    if (focusedScreen && cameraPermission) {
      return this.renderCameraView();
    }
    else if(focusedScreen && !cameraPermission) {
      return this.renderCameraNotGrantedView();
    }
    else{
      return this.renderEmptyView();
    }
  }
}
