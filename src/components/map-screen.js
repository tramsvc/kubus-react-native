import React, { Component } from 'react';
import { Alert, View, Dimensions, Animated, InteractionManager, AppState } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import config from 'config/config';
//Styles
import { styles } from 'styles/map-screen';
//Function Imports
import { TimeFormatter, TimeCalc } from 'utils/time';
//UI Components
import BusInfo from 'map-components/map-bus-info/map-busInfo';
import { BusMarker, StationMarker, NearestBusMarker, UserMarker, RoutePolyline, TargetPolyline } from 'map-components/markerView';
import { BusAndRouteSelectorButton, BusAndRouteSelectorModal, BusPicker, RoutePicker } from 'map-components/bus-route-selector';
import ZoomToTargetBusButton from 'map-components/zoom-targetbus-btn';
import NetNotifier from 'utils/net-notifier';
import ErrorNotifier from 'utils/error-notifier';
//import ErrorNotifier from 'utils/error-notifier';
import StationInfo from 'map-components/station-info-components/station-info.js';
//import { MapSettingButton, SettingModal, DisplayMapSetting } from 'map-setting-components/map-setting';
//Database Services
import { GetBusLocation, GetBusInRoute, GetWaypointInRoute, GetStationLocation, GetRouteInStation, GetAllStationsData, GetStationsDataInRoute } from 'utils/data-fetcher';
import { /*CalcAvgSpeed, CalcDistInKiloMeters,*/ IsUserNearUniversity, ConvertKiloToMeters, FilterBusStep, FindUserNearestStation, /*FindNearestBusByUserLocation,*/ SnapToRoad, FindClosestCoordinateByNode, CalcBusDistanceByNode } from 'utils/new-routing-function';
//Polyline Services
import PolylineData from '../../assets/polylinedata/polyline_data.json';
import { DecodePolyline } from 'utils/decode-google-direction';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const ANCHOR = { x: 0.5, y: 0.5 };
const LATITUDE = config.map.KU_Lat;
const LONGITUDE = config.map.KU_Lon;
const LATITUDE_DELTA = 0.0622;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const INTERVAL_UPDATE_TIME = config.map.updateInterval;
let didFocusSubscription = null;
let didBlurSubscription = null;
//const SETTINGS = [{ name: 'แสดงหมุดรถประจำทาง', value: true }, { name: 'แสดงหมุดสถานี', value: true }, { name: 'แสดงเส้นทางเดินรถประจำสาย (Polyline)', value: true }];

class MapDistScreen extends Component {
    constructor(props) {
        super(props);
        this.watchId = null;
        this.IntervalID = null;
        this._errorTimerID = null;
        this._busInfoAnimatedVal = new Animated.Value(20);
        this._onMapReadyInit = this._onMapReadyInit.bind(this);
        this._stationInfoAnimatedVal = new Animated.Value(0);
        this._handleStationRoutePress = this._handleStationRoutePress.bind(this);
        this._zoomAndShowInfo = this._zoomAndShowInfo.bind(this);
        this.handleBusPickerChanged = this.handleBusPickerChanged.bind(this);
        this.handleRoutePickerChanged = this.handleRoutePickerChanged.bind(this);
        this.fitMapBetweenUserAndTargetBus = this.fitMapBetweenUserAndTargetBus.bind(this);
        this.toggleSelectorModal = this.toggleSelectorModal.bind(this);
        this.IntervalMarkerUpdate = this.IntervalMarkerUpdate.bind(this);
        //this.toggleSettingModal = this.toggleSettingModal.bind(this);
        //this.handleDisplaySettingChanged = this.handleDisplaySettingChanged.bind(this);
        this.state = {
            appState: AppState.currentState,
            focusedScreen: true,
            isSelectorModalVisible: false,
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            busMarkers: [],
            targetBus: null,
            routeSelected: null,
            userCoord: null,
            tappedStation: null,
            markedStation: null,
            routeInStationData: [],
            stationMarkers: [],
            stepData: [],
            decodedPolyline: [],
            targetPolyline: null,
            resultFrom: null,
            error: null
            /*
            startTrackingTime: null,
            totalDistance: 0,
             isSettingModalVisible: false,
             switchOptions: SETTINGS,
             */
        }
    }
    componentDidMount() {
        const { navigation } = this.props;
        didFocusSubscription = navigation.addListener('focus', () => {
            this.setState({ focusedScreen: true })
        });
        didBlurSubscription = navigation.addListener('blur', () => {
            this.setState({ focusedScreen: false })
        });
        AppState.addEventListener('change', this._handleAppStateChange);
    }

    /*
    componentDidUpdate(prevProps, prevState) {
        if (this.state.targetBus && prevState.targetBus && this.state.targetBus !== prevState.targetBus) {
            if (this.state.targetBus.bus_id === prevState.targetBus.bus_id) {
                let nextTargetBusData = this.state.targetBus;
                const oldCoordinate = { latitude: prevState.targetBus.latitude, longitude: prevState.targetBus.longitude };
                const newCoordinate = { latitude: this.state.targetBus.latitude, longitude: this.state.targetBus.longitude };
                const distance = CalcDistInKiloMeters(oldCoordinate.latitude, oldCoordinate.longitude, newCoordinate.latitude, newCoordinate.longitude);
                console.log(oldCoordinate);
                console.log(newCoordinate);
                console.log(`prevState.totalDistance: ${prevState.totalDistance}`);
                console.log(`this.state.totalDistance: ${this.state.totalDistance}`);
                const totalDistance = this.state.totalDistance + distance;
                nextTargetBusData.speed = CalcAvgSpeed(this.state.startTrackingTime, totalDistance);
                let waitTime = TimeFormatter(TimeCalc(this.state.targetBus.distance, nextTargetBusData.speed));
                nextTargetBusData['waitTime'] = waitTime;
                this.setState({ targetBus: nextTargetBusData, totalDistance: totalDistance });
            }
        }
    }
    */

    async _onMapReadyInit() {
        if (this.state.focusedScreen) {
            this.props.navigation.setParams({ toggleSelectorModal: this.toggleSelectorModal });
            const { routeSelected, resultFrom, receivedCoordinate, nearestStation } = this.props.route.params;
            //You can set this in state to use in component-wide and automate detect user is in proximity.
            const isNear = IsUserNearUniversity(receivedCoordinate);
            this.setState({ userCoord: receivedCoordinate, routeSelected: routeSelected, resultFrom: resultFrom, markedStation: nearestStation });
            this.InitState(routeSelected, resultFrom, isNear);
            if (isNear) {
                this.UpdateLocationByGPS(resultFrom);
            }
            this._setIntervalUpdate(INTERVAL_UPDATE_TIME);
        }
    }

    componentWillUnmount() {
        if (typeof (this.IntervalID) !== 'undefined')
            this._clearIntervalUpdate();
        if (typeof (this._errorTimerID) !== 'undefined' || this._errorTimerID !== null)
            clearTimeout(this._errorTimerID);
        if (this.watchId !== null)
            Geolocation.clearWatch(this.watchId);
        AppState.removeEventListener('change', this._handleAppStateChange);
        didFocusSubscription();
        didBlurSubscription();
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this._setIntervalUpdate(INTERVAL_UPDATE_TIME);
        }
        else {
            if (this.IntervalID !== null) {
                this._clearIntervalUpdate();
            }
        }
        this.setState({ appState: nextAppState });
    }

    fitMapBetweenUserAndTargetBus(targetBus, markedStation, userCoord) {
        this._map.fitToCoordinates([userCoord, { latitude: markedStation.latitude, longitude: markedStation.longitude }, { latitude: targetBus.latitude, longitude: targetBus.longitude }], {
            edgePadding: {
                top: 150,
                left: 100,
                right: 150,
                bottom: 600
            }, animated: true
        });
    }

    async updateBusLocation() {
        const { routeSelected } = this.state;

        let result = null;
        if (routeSelected !== null) {
            result = await GetBusInRoute(routeSelected);
        }
        else {
            result = await GetBusLocation();
        }
        let rearrangeBusLocation = await SnapToRoad(result, this.state.decodedPolyline);
        this.setState({ busMarkers: rearrangeBusLocation });
    }

    async updateStation(route) {
        let stationData = null;
        let stepData = null;
        if (route) {
            stationData = await GetStationsDataInRoute(route);
            stepData = await GetWaypointInRoute(route);
        }
        else {
            stationData = await GetAllStationsData();
            stepData = await GetStationLocation();
        }
        this.setState({ stationMarkers: stationData, stepData: stepData });
    }

    async updateTargetBus(busData, targetBusState, markedStation) {
        if (targetBusState) {
            let tappedBus = await busData.find(function (element) {
                if (element.bus_id === targetBusState.bus_id)
                    return element;
            });
            if (typeof (tappedBus) === 'undefined' || !tappedBus.is_active) {
                const FILTERED_STEP_BUSDATA = FilterBusStep(busData, [markedStation], this.state.stepData, this.state.routeSelected);
                tappedBus = FindClosestCoordinateByNode(PolylineData, this.state.decodedPolyline, this.state.routeSelected, [markedStation], FILTERED_STEP_BUSDATA);
                let waitTime = TimeFormatter(TimeCalc(tappedBus.distance, tappedBus.speed));
                tappedBus['waitTime'] = waitTime;
                tappedBus['displayDistance'] = ConvertKiloToMeters(tappedBus.distance);
                this.setState({ targetBus: tappedBus, targetPolyline: tappedBus.targetPolyline });
                return;
            }
            const calResult = CalcBusDistanceByNode(PolylineData, this.state.decodedPolyline, this.state.routeSelected, tappedBus.step, markedStation.step, tappedBus);
            tappedBus['distance'] = calResult.distance;
            let waitTime = TimeFormatter(TimeCalc(tappedBus.distance, tappedBus.speed));
            tappedBus['waitTime'] = waitTime;
            tappedBus['displayDistance'] = ConvertKiloToMeters(tappedBus.distance);
            this.setState({ targetBus: tappedBus, targetPolyline: calResult.targetPolyline });
        }
        else {
            return;
        }
    }
    async InitState(selectedRoute, resultFrom, isNearUniversity) {
        try {
            let busData = null;
            let stationsData = [];
            let stepData = null;
            let NEAREST_BUS = null;
            let markedStation = null;
            if (selectedRoute !== null) {
                busData = await GetBusInRoute(selectedRoute);
                stationsData = await GetStationsDataInRoute(selectedRoute);
                stepData = await GetWaypointInRoute(selectedRoute);
            }
            else {
                busData = await GetBusLocation();
                stationsData = await GetAllStationsData();
                stepData = await GetStationLocation();
            }
            const POLYLINE = await DecodePolyline(PolylineData, selectedRoute);
            this.setState({ busMarkers: busData, stationMarkers: stationsData, stepData: stepData, decodedPolyline: POLYLINE });
            if (resultFrom !== 'qrcode' && !isNearUniversity) {
                return;
            }
            /* Refactor reduce calculation, grab neareststation var from previous page instead */
            let STATION_NEAR_USER = stepData.filter((element) => {
                if (element.station_id === this.state.markedStation.station_id && element.route_id === selectedRoute) {
                    return element;
                }
            });
            /* End Refactor */
            const FILTERED_STEP_BUSDATA = FilterBusStep(busData, STATION_NEAR_USER, stepData, selectedRoute);
            /*
            NEAREST_BUS = FindNearestBusByStations(STATION_NEAR_USER, FILTERED_STEP_BUSDATA, selectedRoute);
            if (STATION_NEAR_USER.length > 1) {
                for (let i = 0; i < STATION_NEAR_USER.length; i++) {
                    if (NEAREST_BUS.step <= STATION_NEAR_USER[i].step) {
                        markedStation = STATION_NEAR_USER[i];
                        break;
                    }
                }
            }
            else {
                markedStation = STATION_NEAR_USER[0];
            }*/
            NEAREST_BUS = FindClosestCoordinateByNode(PolylineData, POLYLINE, selectedRoute, STATION_NEAR_USER, FILTERED_STEP_BUSDATA);
            markedStation = STATION_NEAR_USER[NEAREST_BUS.nearestStationIndex];
            /*
            const tempCalcNodeResult = CalcBusDistanceByNode(PolylineData, POLYLINE, selectedRoute, NEAREST_BUS.step, markedStation.step, NEAREST_BUS);
            NEAREST_BUS.distance = tempCalcNodeResult.distance;
            */
            NEAREST_BUS['waitTime'] = TimeFormatter(TimeCalc(NEAREST_BUS.distance, NEAREST_BUS.speed));
            NEAREST_BUS['displayDistance'] = ConvertKiloToMeters(NEAREST_BUS.distance);
            //const trackingTime = new Date();
            this.setState({ /*startTrackingTime: trackingTime,*/ targetBus: NEAREST_BUS, markedStation: markedStation, targetPolyline: NEAREST_BUS.targetPolyline }, this._zoomAndShowInfo(NEAREST_BUS, markedStation, this.state.userCoord));
        }

        catch (e) {
            this._invokeErrorNotifier(e.message);
            Alert.alert('Error', e.message, [
                { text: "กลับหน้าแรก", onPress: () => this.props.navigation.popToTop() },
                { text: 'ลองอีกครั้ง', onPress: () => this.InitState(selectedRoute) }
            ]);
        }
    }
    _zoomAndShowInfo(target, markedStation, user) {
        if (this.state.focusedScreen && target !== null) {
            InteractionManager.runAfterInteractions(() => {
                this.fitMapBetweenUserAndTargetBus(target, markedStation, user);
                this._busInfoPanel.show();
            })
        }
    }
    _RetryConnection = (e) => {
        Alert.alert('Error', e.message, [
            { text: "กลับหน้าแรก", onPress: () => this.props.navigation.popToTop() },
            { text: 'ลองอีกครั้ง', onPress: () => this._setIntervalUpdate(INTERVAL_UPDATE_TIME) }]);
    }
    _setIntervalUpdate(intervalTime) {
        this.IntervalID = setTimeout(this.IntervalMarkerUpdate, intervalTime, intervalTime);
    }
    _clearIntervalUpdate() {
        clearTimeout(this.IntervalID);
        this.IntervalID = undefined;
    }
    async IntervalMarkerUpdate(intervalTime) {
        try {
            if (this.state.focusedScreen) {
                await this.updateBusLocation();
                await this.updateTargetBus(this.state.busMarkers, this.state.targetBus, this.state.markedStation);
                this._setIntervalUpdate(intervalTime);
            }
        }
        catch (e) {
            this._invokeErrorNotifier(e.message);
            this._RetryConnection(e);
        }
    }

    UpdateLocationByGPS = async (resultFrom) => {
        if (resultFrom === 'gps') {
            this.watchId = Geolocation.watchPosition(
                (position) => {
                    const { busMarkers, decodedPolyline, markedStation, stepData, routeSelected } = this.state;
                    const coordinate = { latitude: position.coords.latitude, longitude: position.coords.longitude }
                    if (markedStation !== null && stepData.length > 0 && routeSelected !== null) {
                        if (this.IntervalID) {
                            this._clearIntervalUpdate();
                        }
                        const nearest_station = FindUserNearestStation(stepData, coordinate, routeSelected);
                        const filteredBusStep = FilterBusStep(busMarkers, nearest_station, stepData, routeSelected);
                        let nearest_bus = FindClosestCoordinateByNode(PolylineData, decodedPolyline, routeSelected, nearest_station, filteredBusStep);
                        const newMarkedStation = nearest_station[nearest_bus.nearestStationIndex];
                        nearest_bus['waitTime'] = TimeFormatter(TimeCalc(nearest_bus.distance, nearest_bus.speed));
                        nearest_bus['displayDistance'] = ConvertKiloToMeters(nearest_bus.distance);
                        this.setState({
                            userCoord: coordinate,
                            targetBus: nearest_bus,
                            markedStation: newMarkedStation,
                            targetPolyline: nearest_bus.targetPolyline
                        });
                        if (typeof (this.IntervalID) === 'undefined') {
                            this._setIntervalUpdate(INTERVAL_UPDATE_TIME);
                        }
                    }
                    else {
                        this.setState({
                            userCoord: coordinate,
                        });
                    }
                },
                (error) => alert(error.message),
                { enableHighAccuracy: config.gps.useHighAccuracy, timeout: config.gps.watch_timeout, distanceFilter: config.gps.watch_distance_filter, fastestInterval: config.gps.fastInterval }
            );
        }
    }

    /* Marked for removal after test finished.
    //Region MapSetting
    toggleSettingModal() {
        this.setState(prevState => ({ isSettingModalVisible: !prevState.isSettingModalVisible }));
    }
    handleDisplaySettingChanged(options) {
        this.setState({ switchOptions: options });
    }
    */
    toggleSelectorModal() {
        this.setState({ isSelectorModalVisible: !this.state.isSelectorModalVisible });
    }
    async handleRoutePickerChanged(value) {
        let selectedValue = value;
        if (selectedValue && (selectedValue !== this.state.routeSelected)) {
            if (this.IntervalID) {
                this._clearIntervalUpdate();
            }
            await this.updateStation(value);
            const NEW_POLYLINE = await DecodePolyline(PolylineData, selectedValue);
            const NEW_BUSDATA = await GetBusInRoute(selectedValue);
            const STATION_NEAR_USER = await FindUserNearestStation(this.state.stepData, this.state.userCoord, selectedValue);
            const FILTERED_STEP_BUSDATA = await FilterBusStep(NEW_BUSDATA, STATION_NEAR_USER, this.state.stepData, selectedValue);
            let NEAREST_BUS = FindClosestCoordinateByNode(PolylineData, NEW_POLYLINE, selectedValue, STATION_NEAR_USER, FILTERED_STEP_BUSDATA);
            const markedStation = STATION_NEAR_USER[NEAREST_BUS.nearestStationIndex];
            NEAREST_BUS['waitTime'] = TimeFormatter(TimeCalc(NEAREST_BUS.distance, NEAREST_BUS.speed));
            NEAREST_BUS['displayDistance'] = ConvertKiloToMeters(NEAREST_BUS.distance);
            //const trackingTime = new Date();
            /** End block **/
            this.setState({ /*startTrackingTime: trackingTime,*/ busMarkers: NEW_BUSDATA, decodedPolyline: NEW_POLYLINE, routeSelected: selectedValue, targetBus: NEAREST_BUS, targetPolyline: NEAREST_BUS.targetPolyline, markedStation: markedStation },
                this._zoomAndShowInfo(NEAREST_BUS, markedStation, this.state.userCoord)
            );
            if (typeof (this.IntervalID) === 'undefined') {
                this._setIntervalUpdate(INTERVAL_UPDATE_TIME);
            }
        }
    }

    handleBusPickerChanged(value) {
        let bus = this.state.busMarkers.find(function (element) {
            if (element.bus_id === value && element.is_active)
                return element;
        });
        if (typeof (bus) !== "undefined" && bus) {
            const calResult = CalcBusDistanceByNode(PolylineData, this.state.decodedPolyline, this.state.routeSelected, bus.step, this.state.markedStation.step, bus);
            bus['distance'] = calResult.distance;
            let waitTime = TimeFormatter(TimeCalc(bus.distance, bus.speed));
            bus['waitTime'] = waitTime;
            bus['displayDistance'] = ConvertKiloToMeters(bus.distance);
            //const trackingTime = new Date();
            this.setState({ /*startTrackingTime: trackingTime, totalDistance: 0,*/targetBus: bus, targetPolyline: calResult.targetPolyline });
            this._zoomAndShowInfo(bus, this.state.markedStation, this.state.userCoord);
        }
        else return;
    }

    //End region Mapsetting
    RenderBusMarkers() {
        const { busMarkers, /*switchOptions*/ } = this.state;
        if ( /*!switchOptions[0].value ||*/(busMarkers.length === 0 || busMarkers === null)) {
            return null;
        }
        else {
            return busMarkers.map((marker) => {
                if (this.state.targetBus) {
                    let targetState = (marker.bus_id === this.state.targetBus.bus_id ? true : false);
                    return <BusMarker
                        key={marker.bus_id}
                        busData={marker}
                        onPress={(e) => { e.nativeEvent.busInfo = marker; this.onBusMarkerPress(e.nativeEvent) }}
                        isTargetBus={targetState}
                        anchor={ANCHOR} />
                }
                else {
                    return <BusMarker
                        key={marker.bus_id}
                        busData={marker}
                        onPress={(e) => { e.nativeEvent.busInfo = marker; this.onBusMarkerPress(e.nativeEvent) }}
                        isTargetBus={false}
                        anchor={ANCHOR} />
                }
            });
        }
    }
    RenderStationMarkers() {
        const { stationMarkers, markedStation /*switchOptions*/ } = this.state;
        if (!Array.isArray(stationMarkers) || stationMarkers.length === 0 /*,!switchOptions[1].value ||*/) {
            return null;
        }
        else {
            return stationMarkers.map((marker) => {
                let isNearest = marker.station_id === markedStation.station_id ? true : false;
                return <StationMarker
                    data={marker}
                    key={marker.station_id}
                    isNearestStation={isNearest}
                    onPress={(e) => {
                        e.nativeEvent.id = marker.station_id;
                        this.onStationMarkerPress(e.nativeEvent)
                    }}
                />
            });
        }
    }

    RenderTargetPolyline() {
        const { targetPolyline } = this.state;
        if (targetPolyline !== null)
            return <TargetPolyline data={targetPolyline} />;
        else
            return null;
    }

    RenderUserMarker() {
        const { userCoord } = this.state;
        if (userCoord === null) {
            return null;
        }
        else {
            return <UserMarker coordinate={userCoord} />
        }
    }

    RenderTargetBusMarker() {
        const { targetBus, /*switchOptions*/ } = this.state;
        if (/*!switchOptions[0].value ||*/ targetBus === null || typeof (targetBus) === 'undefined')
            return null;
        else
            return <NearestBusMarker data={targetBus} anchor={ANCHOR} />
    }

    RenderPolyline() {
        const { decodedPolyline, /*switchOptions*/ } = this.state;
        if (/*!switchOptions[2].value ||*/ decodedPolyline.length === 0)
            return null;
        else
            return <RoutePolyline data={decodedPolyline} />
    }

    onBusMarkerPress(e) {
        const { busMarkers, markedStation } = this.state;
        this.updateTargetBus(busMarkers, e.busInfo, markedStation);
        //const trackingTime = new Date();
        //this.setState({ startTrackingTime: trackingTime, totalDistance: 0 });
    }

    async onStationMarkerPress(e) {
        const { stationMarkers } = this.state;
        let tappedStation = await stationMarkers.find(function (station) {
            if (station.station_id === e.id)
                return station;
        });
        const routesInStation = await GetRouteInStation(tappedStation.station_id);
        this.setState({ tappedStation: tappedStation, routeInStationData: routesInStation });
        this._StationInfoPanel.show();
        if (this._busInfoAnimatedVal._value > 20) {
            this._busInfoPanel.hide();
        }
    }

    RenderStationInfoPanel() {
        const { tappedStation, routeInStationData } = this.state;
        if (routeInStationData.length > 0 && tappedStation !== null) {
            return (
                <StationInfo
                    panelRef={c => this._StationInfoPanel = c}
                    animatedValue={this._stationInfoAnimatedVal}
                    stationName={tappedStation.station_name}
                    routesData={routeInStationData}
                    onPress={this._handleStationRoutePress}
                />)
        }
    }
    _handleStationRoutePress(routeID) {
        this.handleRoutePickerChanged(routeID);
    }
    _invokeErrorNotifier(errorMessage) {
        this.setState({ error: errorMessage });
        if (this._errorTimerID === null)
            this._errorTimerID = setTimeout(() => {
                this.setState({ error: null });
                this._errorTimerID = null;
            }, 5000);
    }

    render() {
        const { busMarkers, error, region, isSelectorModalVisible, markedStation,/*isSettingModalVisible, switchOptions,*/ routeSelected, targetBus, userCoord } = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.notifier_container}>
                    <NetNotifier />
                    <ErrorNotifier errorMessage={error} />
                </View>
                {
                    <MapView
                        initialRegion={region}
                        provider={PROVIDER_GOOGLE}
                        ref={(m) => { this._map = m }}
                        loadingEnabled={true}
                        zoomEnabled={true}
                        showsCompass={false}
                        style={styles.map}
                        onMapReady={this._onMapReadyInit}
                    >
                        {this.RenderUserMarker()}
                        {this.RenderStationMarkers()}
                        {this.RenderBusMarkers()}
                        {this.RenderTargetBusMarker()}
                        {this.RenderTargetPolyline()}
                        {this.RenderPolyline()}
                    </MapView>
                }
                <BusInfo panelReference={c => this._busInfoPanel = c} animatedValue={this._busInfoAnimatedVal} targetBusData={this.state.targetBus} />
                {this.RenderStationInfoPanel()}
                <BusAndRouteSelectorModal showSelectorModal={isSelectorModalVisible} title={'การตั้งค่ารถและเส้นทาง'} onModalToggle={this.toggleSelectorModal} >
                    <RoutePicker routeSelected={routeSelected} onRoutePickerChangeHandler={this.handleRoutePickerChanged} />
                    <BusPicker busData={busMarkers} routeSelected={routeSelected} selectedBus={targetBus} onBusPickerChangedHandler={this.handleBusPickerChanged} />
                </BusAndRouteSelectorModal>
                <View style={styles.optionsRightSidepane}>
                    <BusAndRouteSelectorButton onPressHandler={this.toggleSelectorModal} />
                    <ZoomToTargetBusButton onPressHandler={this._zoomAndShowInfo.bind(this, targetBus, markedStation, userCoord)} />
                    {
                        /*
                        <SettingModal showSettingModal={isSettingModalVisible} title={'การตั้งค่าแผนที่'} onModalToggle={this.toggleSettingModal}>
                            <DisplayMapSetting options={switchOptions} onSettingChangeHandler={this.handleDisplaySettingChanged} />
                        </SettingModal>
                        <MapSettingButton onPressHandler={this.toggleSettingModal} />
                        */
                    }
                </View>
            </View>
        )
    }
}
export default MapDistScreen;
