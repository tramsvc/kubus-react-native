import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, Animated } from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faGripLines } from '@fortawesome/free-solid-svg-icons';

export default class SlideUpPanel extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const iconOpacity = this.props.animatedValue.interpolate({
            inputRange: [20, this.props.height - 110],
            outputRange: [1, 0],
            extrapolate: "clamp"
        });
        return (
            <SlidingUpPanel ref={this.props.reference} animatedValue={this.props.animatedValue} showBackdrop={this.props.showBackdrop} height={this.props.height} draggableRange={this.props.draggableRange}>
                <View style={styles.panelContainer}>
                    <View style={styles.draggableContainer}>
                        <Animated.View style={[styles.draggableIcon, { opacity: iconOpacity }]}>
                            <FontAwesomeIcon icon={faGripLines} color={'white'} />
                        </Animated.View>
                    </View>
                    <View style={this.props.subtitle ? styles.titleContainer : styles.titleContainerWithoutSubtitle}>
                        <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                            <FontAwesomeIcon style={{ marginRight: 15 }} color={'#fff'} icon={this.props.icon} size={20} />
                            <Text style={styles.titleBarText}>{this.props.title} </Text>
                        </View>
                        {
                            this.props.subtitle !== null &&
                            <Text style={styles.subtitleBarText} numberOfLines={1}>{this.props.subtitle}</Text>
                        }

                    </View>
                    {this.props.children}
                </View>
            </SlidingUpPanel>
        )

    }
}

const styles = StyleSheet.create({
    panelContainer: {
        flex: 1,
        backgroundColor: '#1ebdaf'
    },
    titleContainer: {
        flexDirection: 'column',
        overflow:'scroll',
        backgroundColor: '#20c9bb',
        paddingHorizontal: 15,
        paddingBottom: 10,
        ...Platform.select({
            ios: {
                shadowColor: "rgba(0,0,0,0.11)",
                shadowOffset: {
                    width: 1.5,
                    height: 1.5
                },
                shadowRadius: 6,
                shadowOpacity: 1
            },
            android: {
                elevation: 2,
            }
        })
    },
    titleContainerWithoutSubtitle: {
        flexDirection: 'column',
        backgroundColor: '#20c9bb',
        paddingHorizontal: 15,
        paddingBottom: 5,
        ...Platform.select({
            ios: {
                shadowColor: "rgba(0,0,0,0.11)",
                shadowOffset: {
                    width: 1.5,
                    height: 1.5
                },
                shadowRadius: 6,
                shadowOpacity: 1
            },
            android: {
                elevation: 2,
            }
        })
    },
    titleBarText: {
        fontSize: 18,
        color: '#fff',
        fontWeight: 'bold'
    },
    subtitleBarText: {
        fontSize: 16,
        color: '#fff',
        fontWeight: 'bold'
    },
    draggableContainer: {
        backgroundColor: '#20c9bb',
        paddingTop: 5,
        height: 20
    },
    draggableIcon: {
        justifyContent: 'center',
        alignSelf: 'center'
    }
})