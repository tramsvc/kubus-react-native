import React from 'react';
import { View, Text, StyleSheet, Switch } from 'react-native';

export default function StyledSwitch(props) {
    return (
        <View>
            <View style={styles.switchRowContainer}>
                <View style={styles.switchRowText}>
                    <Text>{props.text}</Text>
                </View>
                <View style={styles.switchStyle}>
                    <Switch value={props.value} onValueChange={props.valueChangeFunc} />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    switchRowContainer: {
        marginTop: 10,
        flexDirection: 'row',
    },
    switchRowText: {
        flex: 1,
        justifyContent: 'flex-start',
        marginLeft: 30,
    },
    switchStyle: {
        flex: 1,
        justifyContent: 'flex-end',
        marginRight: 30,
    },
})