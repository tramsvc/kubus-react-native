import React, { Component } from 'react';
import { View, Text, Dimensions, Animated, Alert } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import { withNavigationFocus } from 'react-navigation';
//Style
import { styles } from 'styles/map-screen';
//Function Imports
import { TimeFormatter, TimeCalc } from 'utils/time';
import GenerateLatLng from 'utils/generateLatLng';
//UI Components
import BusInfo from 'map-components/map-bus-info/map-busInfo';
import { BusMarker, StationMarker, NearestBusMarker, UserMarker, RoutePolyline } from 'map-components/markerView';
import { BusAndRouteSelectorButton, BusAndRouteSelectorModal, BusPicker, RoutePicker } from 'map-components/bus-route-selector';
import { SettingModal, MapSettingButton, DisplayMapSetting } from 'map-setting-components/map-setting';
import StationInfo from './map-components/station-info-components/station-info';
import NetNotifier from 'utils/net-notifier';

//Database Services
import { GetBusLocation, GetBusInRoute, GetWaypointInRoute, GetStationLocation, GetRouteInStation } from 'utils/data-fetcher';
import { IsUserNearUniversity, ConvertKiloToMeters, FilterBusStep, FindNearestBusByStations, FindUserNearestStation, FindNearestBusByUserLocation, FindClosestCoordinate, SnapToRoad } from 'utils/new-routing-function';
//Polyline Services
import PolylineData from '../../assets/polylinedata/polyline_data.json';
import { DecodePolyline } from 'utils/decode-google-direction';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const ANCHOR = { x: 0.5, y: 0.5 };
const INTERVAL_UPDATE_TIME = 4000;
const LATITUDE = 14.0236026;
const LATITUDE_DELTA = 0.0622;
const LONGITUDE = 99.9748993;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SETTINGS = [{ name: 'แสดงหมุดรถประจำทาง', value: true }, { name: 'แสดงหมุดสถานี', value: true }, { name: 'แสดงเส้นทางเดินรถประจำสาย (Polyline)', value: true }];

//Simulation Const
const LAT_MIN_BOUND = 14.030540;
const LAT_MAX_BOUND = 14.019529;
const LON_MIN_BOUND = 99.970509;
const LON_MAX_BOUND = 99.991781;

class MarkerSimulation extends Component {
    static navigationOptions = () => {
        return {
            headerTransparent: true
        }
    }
    constructor(props) {
        super(props);
        this.screenMounted = true;
        this.watchId = null;
        this.IntervalID = null;
        this._busInfoAnimatedVal = new Animated.Value(0);
        this._stationInfoAnimatedVal = new Animated.Value(0);

        this.fitMapBetweenUserAndTargetBus = this.fitMapBetweenUserAndTargetBus.bind(this);
        this.handleBusPickerChanged = this.handleBusPickerChanged.bind(this);
        this.handleDisplaySettingChanged = this.handleDisplaySettingChanged.bind(this);
        this.handleRoutePickerChanged = this.handleRoutePickerChanged.bind(this);
        this.toggleSelectorModal = this.toggleSelectorModal.bind(this);
        this.toggleSettingModal = this.toggleSettingModal.bind(this);
        this.state = {
            isSettingModalVisible: false,
            isSelectorModalVisible: false,
            switchOptions: SETTINGS,
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            isNativeUserLocationShow: false,
            busMarkers: [],
            targetBus: null,
            routeSelected: null,
            userCoord: null,
            stationMarkers: [],
            tappedStation: null,
            routeInStationData: [],
            decodedPolyline: [],
            resultFrom: null,
            error: null
        }
    }

    async componentDidMount() {
        this.screenMounted = true;
        const ROUTE_SELECTED = this.props.navigation.getParam('routeSelected');
        const RESULT_SOURCE = this.props.navigation.getParam('resultFrom');
        const USER_COORDINATE = this.props.navigation.getParam('receivedCoordinate');
        this.setState({ userCoord: USER_COORDINATE, routeSelected: ROUTE_SELECTED, resultFrom: RESULT_SOURCE });
        await this.InitState(ROUTE_SELECTED);
        await this.UpdateLocationByGPS();
        if (this.state.targetBus !== null) {
            await this.delay(1000).then(() => { this._busInfoPanel.show(); });
        }
        await this.callIntervalMarkerUpdate(INTERVAL_UPDATE_TIME);
    }
    componentWillUnmount() {
        this.screenMounted = false;
        if (this.intervalID) {
            clearTimeout(this.intervalID);
        }
        if (this.watchId) {
            Geolocation.clearWatch(this.watchId);
        }
        this.props.navigation.popToTop();
    }

    fitMapBetweenUserAndTargetBus() {
        const { userCoord, targetBus } = this.state;
        if (targetBus !== null) {
            setTimeout(() => {
                this._map.fitToCoordinates([userCoord, { latitude: targetBus.latitude, longitude: targetBus.longitude }], {
                    edgePadding: {
                        top: 100,
                        right: 100,
                        bottom: 300,
                        left: 100
                    }, animated: true
                });
            }, 500);
        }
    }

    
    async setTimeout(ms) {
        return new Promise(resolve => { this.intervalID = setTimeout(resolve, ms) });
    }
    async delay(ms) {
        return new Promise(resolve => { setTimeout(resolve, ms) });
    }
    
    async updateBusLocation() {
        const { routeSelected } = this.state;
        let busData = await GetBusInRoute(routeSelected);
        let newBusMarkers = busData.map((marker) => {
            let randomLatLngSet = GenerateLatLng(LAT_MIN_BOUND, LAT_MAX_BOUND, LON_MIN_BOUND, LON_MAX_BOUND);
            marker.latitude = randomLatLngSet.lat;
            marker.longitude = randomLatLngSet.lng;
            return marker;
        });
        const rearrangeBusLocation = SnapToRoad(newBusMarkers, this.state.decodedPolyline);
        this.setState({ busMarkers: rearrangeBusLocation });
    }
    async updateStation(route) {
        let stationData;
        if (route === null)
        stationData = await GetStationLocation();
        else
        stationData = await GetWaypointInRoute(route);
        this.setState({ stationMarkers: stationData });
    }
    
    async updateTargetBus(busData, targetBusState, userCoord) {
        if (targetBusState) {
            let tappedBus = await busData.find(function (element) {
                if (element.bus_id === targetBusState.bus_id)
                return element;
            });
            tappedBus = FindClosestCoordinate(tappedBus, userCoord);
            let waitTime = TimeFormatter(TimeCalc(tappedBus.distance, tappedBus.speed));
            tappedBus['waitTime'] = waitTime;
            tappedBus['displayDistance'] = ConvertKiloToMeters(tappedBus.distance);
            this.setState({ targetBus: tappedBus });
        }
        else {
            return;
        }
    }
    
    async InitState(selectedRoute = 1) {
        try {
            let busData = null;
            let stationsData = null
            let shouldUseNativeUserLocation = this.state.resultFrom === 'qrcode' ? true : false;
            //Fetch BusLocation
            if (selectedRoute === null) {
                busData = await GetBusLocation();
                stationsData = await GetStationLocation();
            }
            else {
                busData = await GetBusInRoute(selectedRoute);
                stationsData = await GetWaypointInRoute(selectedRoute);
            }
            const POLYLINE = DecodePolyline(PolylineData, selectedRoute);
            this.setState({ busMarkers: busData, stationMarkers: stationsData, decodedPolyline: POLYLINE, isNativeUserLocationShow: shouldUseNativeUserLocation });
            if (this.state.resultFrom !== 'qrcode' && !IsUserNearUniversity(this.state.userCoord)) {
                return;
            }
            const STATION_NEAR_USER = FindUserNearestStation(stationsData, this.state.userCoord, selectedRoute);
            //console.log("STATION_NEAR_USER");
            //console.log(STATION_NEAR_USER);
            const FILTERED_STEP_BUSDATA = FilterBusStep(busData, STATION_NEAR_USER, selectedRoute);
            //console.log("FILTERED_STEP_BUSDATA");
            //console.log(FILTERED_STEP_BUSDATA);
            const NEAREST_BUS = FindNearestBusByStations(STATION_NEAR_USER, FILTERED_STEP_BUSDATA, selectedRoute);
            //console.log("NEAREST_BUS");
            //console.log(NEAREST_BUS);
            this.setState({ targetBus: NEAREST_BUS });
        }
        catch (e) {
            this._RetryConnection(e);
        }
    }
    
    async callIntervalMarkerUpdate(intervalTime) {
        if (this.screenMounted) {
            await this.setTimeout(intervalTime).then(() => {
                this.IntervalMarkerUpdate(intervalTime);
            });
        }
    }
    async IntervalMarkerUpdate(intervalTime = 10000) {
        try {
            console.log("Running Update Interval");
            if (this.screenMounted) {
                await this.updateBusLocation();
                await this.updateTargetBus(this.state.busMarkers, this.state.targetBus, this.state.userCoord);
                await this.callIntervalMarkerUpdate(intervalTime);

            }
        }
        catch (e) {
            console.error(e);
            this._RetryConnection(e);
        }
    }

    _RetryConnection = (e) => {
        this.setState({ error: e.message }, Alert.alert('Error', e.message, [{ text: 'OK' }]));
        console.error(e);
        this.IntervalMarkerUpdate(INTERVAL_UPDATE_TIME);
    }

    async UpdateLocationByGPS() {
        if (this.state.resultFrom === 'gps') {
            this.watchId = Geolocation.watchPosition(
                (position) => {
                    this.setState({
                        userCoord: {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                        }
                    });
                    this.updateTargetBus(this.state.busMarkers, this.state.targetBus, position.coords);
                },
                (error) => this.setState({ error: error.message }),
                { enableHighAccuracy: true, timeout: 10000, distanceFilter: 50 },
            );
        }
    }

    //Region MapSetting
    toggleSettingModal() {
        this.setState({ isSettingModalVisible: !this.state.isSettingModalVisible });
    }
    toggleSelectorModal() {
        this.setState({ isSelectorModalVisible: !this.state.isSelectorModalVisible });
    }
    handleDisplaySettingChanged(options) {
        this.setState({ switchOptions: options });
    }

    async handleRoutePickerChanged(value) {
        let selectedValue = value;
        if (selectedValue !== null) {
            const newPolyline = await DecodePolyline(PolylineData, selectedValue);
            const newBusData = await GetBusInRoute(selectedValue);
            const nearestBus = FindNearestBusByUserLocation(newBusData, this.state.userCoord);
            this.updateStation(value);
            this.setState({ busMarkers: newBusData, decodedPolyline: newPolyline, routeSelected: value, targetBus: nearestBus });
        }
    }
    async handleBusPickerChanged(value) {
        this.setState({ targetBus: value });
    }
    //End Region Mapsetting

    RenderBusMarkers() {
        const { busMarkers, switchOptions } = this.state;
        if (!switchOptions[0].value || (busMarkers.length === 0 || busMarkers === null)) {
            return null;
        }
        else {
            return busMarkers.map((marker) => {
                return <BusMarker
                    busData={marker}
                    onPress={(e) => { e.nativeEvent.id = marker.bus_id; this.onBusMarkerPress(e.nativeEvent) }}
                    anchor={ANCHOR} />
            });
        }
    }

    RenderStationMarkers() {
        const { stationMarkers, switchOptions } = this.state;
        if (!switchOptions[1].value || stationMarkers.length === 0) {
            return null;
        }
        else {
            return stationMarkers.map((marker) => {
                return <StationMarker
                    data={marker}
                    onPress={(e) => {
                        e.nativeEvent.id = marker.station_id;
                        this.onStationMarkerPress(e.nativeEvent)
                    }}
                />
            });
        }
    }

    RenderUserMarker() {
        const { userCoord, resultFrom } = this.state;
        if (resultFrom === 'qrcode' || userCoord === null)
            return null;
        else
            return <UserMarker coordinate={userCoord} />
    }

    RenderTargetBusMarker() {
        const { targetBus, switchOptions } = this.state;
        if (!switchOptions[0].value || targetBus === null || targetBus === undefined)
            return null;
        else
            return <NearestBusMarker data={targetBus} anchor={ANCHOR} />
    }

    RenderPolyline() {
        const { decodedPolyline, switchOptions } = this.state;
        if (!switchOptions[2].value || decodedPolyline.length === 0)
            return null;
        else
            return <RoutePolyline data={decodedPolyline} />
    }

    async onBusMarkerPress(e) {
        const { busMarkers, routeSelected, userCoord } = this.state;
        let tappedBus = null;
        if (busMarkers.length > 0) {
            tappedBus = await busMarkers.find((element) => {
                if (element.bus_id === e.id && element.route_id === routeSelected)
                    return element;
            });
            if (tappedBus.is_active) {
                tappedBus = FindNearestBusByUserLocation(tappedBus, userCoord);
                this.setState({ targetBus: tappedBus });
                this.fitMapBetweenUserAndTargetBus();
                this._busInfoPanel.show();
                this._StationInfoPanel.hide();
            }
        }
    }

    async onStationMarkerPress(e) {
        const { stationMarkers } = this.state;
        let tappedStation = await stationMarkers.find(function (station) {
            if (station.station_id === e.id)
                return station;
        });
        const routesInStation = await GetRouteInStation(tappedStation.station_id);
        this.setState({ tappedStation: tappedStation, routeInStationData: routesInStation });
        this._StationInfoPanel.show();
        this._busInfoPanel.hide();
    }

    RenderMapInfoSlidePanel() {
        const { targetBus } = this.state;
        if (targetBus !== null)
            return <BusInfo panelReference={c => this._busInfoPanel = c} animatedValue={this._busInfoAnimatedVal} targetBusData={this.state.targetBus} />
    }

    RenderStationInfoPanel() {
        const { tappedStation, routeInStationData } = this.state;
        if (routeInStationData.length > 0 && tappedStation !== null) {
            return (
                <StationInfo
                    panelRef={c => this._StationInfoPanel = c}
                    animatedValue={this._stationInfoAnimatedVal}
                    stationName={tappedStation.station_name}
                    routesData={routeInStationData}
                />)
        }
    }

    RenderErrorText() {
        if (this.state.error !== null)
            return (
                <Text style={{ color: 'red', textAlign: 'center' }}>Error: {this.state.error.message}</Text>
            );
    }


    render() {
        const { busMarkers, region, isNativeUserLocationShow, isSelectorModalVisible, isSettingModalVisible, switchOptions, routeSelected, targetBus } = this.state;
        return (
            <View style={styles.container}>
                {this.RenderErrorText()}
                <NetNotifier />
                <MapView
                    followsUserLocation={isNativeUserLocationShow}
                    initialRegion={region}
                    onLayout={this.fitMapBetweenUserAndTargetBus}
                    provider={PROVIDER_GOOGLE}
                    ref={map => { this._map = map }}
                    showsCompass={false}
                    showsUserLocation={isNativeUserLocationShow}
                    style={styles.map}
                >
                    {this.RenderUserMarker()}
                    {this.RenderStationMarkers()}
                    {this.RenderBusMarkers()}
                    {this.RenderPolyline()}
                    {this.RenderTargetBusMarker()}
                </MapView>
                <SettingModal showSettingModal={isSettingModalVisible} title={'การตั้งค่าแผนที่'} onModalToggle={this.toggleSettingModal}>
                    <DisplayMapSetting options={switchOptions} onSettingChangeHandler={this.handleDisplaySettingChanged} />
                </SettingModal>
                <BusAndRouteSelectorModal showSelectorModal={isSelectorModalVisible} title={'การตั้งค่ารถและเส้นทาง'} onModalToggle={this.toggleSelectorModal} >
                    <RoutePicker routeSelected={routeSelected} onRoutePickerChangeHandler={this.handleRoutePickerChanged} />
                    <BusPicker busData={busMarkers} routeSelected={routeSelected} selectedBus={targetBus} onBusPickerChangedHandler={this.handleBusPickerChanged} />
                </BusAndRouteSelectorModal>
                {this.RenderMapInfoSlidePanel()}
                {this.RenderStationInfoPanel()}
                <View style={styles.optionsRightSidepane}>
                    <MapSettingButton onPressHandler={this.toggleSettingModal} />
                    <BusAndRouteSelectorButton onPressHandler={this.toggleSelectorModal} />
                </View>
            </View>
        )
    }
}
export default withNavigationFocus(MarkerSimulation);