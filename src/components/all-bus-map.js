import React, { Component } from 'react';
import { Animated, AppState, Dimensions, InteractionManager, View } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import config from 'config/config';
//Styles
import { styles } from 'styles/map-screen';
//UI Components
import { BusMarker, StationMarker, RoutePolyline } from 'map-components/markerView';
import NetNotifier from 'utils/net-notifier';
import StationInfo from 'map-components/station-info-components/station-info.js';
//Database Services
import { GetBusLocation, GetStationLocation, GetRouteInStation, GetAllStationsData } from 'utils/data-fetcher';
//Polyline Services
import PolylineData from '../../assets/polylinedata/polyline_data.json';
import { DecodePolyline } from 'utils/decode-google-direction';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const ANCHOR = { x: 0.5, y: 0.5 };
const LATITUDE = config.map.KU_Lat;
const LONGITUDE = config.map.KU_Lon;
const LATITUDE_DELTA = 0.0622;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const INTERVAL_UPDATE_TIME = config.map.updateInterval;

class AllBusMap extends Component {
    constructor(props) {
        super(props);
        this._map = null;
        this.screenMounted = true;
        this.IntervalID = null;
        this._stationInfoAnimatedVal = new Animated.Value(0);
        this.IntervalMarkerUpdate = this.IntervalMarkerUpdate.bind(this);
        this._onStationInfoRowPress = this._onStationInfoRowPress.bind(this);
        this._onMapReadyInit = this._onMapReadyInit.bind(this);
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            appState: AppState.currentState,
            busMarkers: [],
            targetBus: null,
            stationMarkers: [],
            decodedPolyline: [],
            tappedStation: null,
            routeInStationData: [],
            error: null
        }
    }
    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
        this.screenMounted = true;
        this.InitState();
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        this.screenMounted = false;
        if (this.IntervalID)
            this._clearIntervalUpdate();
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this._setIntervalUpdate(INTERVAL_UPDATE_TIME);
        }
        else {
            if (this.IntervalID !== null) {
                this._clearIntervalUpdate();
            }
        }
        this.setState({ appState: nextAppState });
    }

    async updateBusLocation() {
        let result = null;
        result = await GetBusLocation();
        this.setState({ busMarkers: result });
    }

    _onMapReadyInit() {
        this._setIntervalUpdate(INTERVAL_UPDATE_TIME);
    }

    _setIntervalUpdate(intervalTime) {
        this.IntervalID = setTimeout(this.IntervalMarkerUpdate, intervalTime, intervalTime);
    }
    _clearIntervalUpdate() {
        clearTimeout(this.IntervalID);
        this.IntervalID = undefined;
    }

    async InitState() {
        try {
            let busData = null;
            let stationsData = null;
            busData = await GetBusLocation();
            stationsData = await GetAllStationsData();
            stepData = await GetStationLocation();
            const POLYLINE = await DecodePolyline(PolylineData);
            this.setState({ busMarkers: busData, stationMarkers: stationsData, stepData: stepData, decodedPolyline: POLYLINE });
        }
        catch (e) {
            this._RetryConnection(e);
        }
    }

    async IntervalMarkerUpdate(intervalTime) {
        try {
            if (this.screenMounted) {
                await this.updateBusLocation();
                this._setIntervalUpdate(intervalTime);
            }
        }
        catch (e) {
            this._RetryConnection(e);
        }
    }

    _RetryConnection = () => {
        this._setIntervalUpdate(INTERVAL_UPDATE_TIME);
    }

    _renderBusMarkers() {
        const { busMarkers } = this.state;
        if (busMarkers.length === 0 || busMarkers === null) {
            return null;
        }
        else {
            return busMarkers.map((marker) => {
                return <BusMarker
                    key={marker.bus_id}
                    busData={marker}
                    onPress={(e) => { e.nativeEvent.id = marker.bus_id; this.onBusMarkerPress(e.nativeEvent) }}
                    isTargetBus={false}
                    anchor={ANCHOR} />
            });
        }
    }
    _renderStationMarkers() {
        const { stationMarkers } = this.state;
        if (stationMarkers.length === 0) {
            return null;
        }
        else {
            return stationMarkers.map((marker) => {
                return <StationMarker
                    key={marker.station_id}
                    data={marker}
                    onPress={(e) => {
                        e.nativeEvent.id = marker.station_id;
                        this.onStationMarkerPress(e.nativeEvent)
                    }}
                />
            });
        }
    }

    _renderPolyline() {
        const { decodedPolyline, targetBus } = this.state;
        if (decodedPolyline.routeType === 'multiple' && targetBus === null) {
            return decodedPolyline.map((e) => {
                return <RoutePolyline data={e.coords} color={e.color} />
            });
        }
        else if (decodedPolyline.routeType === 'single' && targetBus !== null) {
            return <RoutePolyline data={decodedPolyline} color={targetBus.color} />
        }
        else if (decodedPolyline.routeType === 'single') {
            return <RoutePolyline data={decodedPolyline} color={decodedPolyline.color} />
        }
        else
            return null;
    }
    onBusMarkerPress(e) {
        const { busMarkers } = this.state;
        let tappedBus = null;
        if (busMarkers.length > 0) {
            tappedBus = busMarkers.find((element) => {
                if (element.bus_id === e.id)
                    return element;
            });
            if (tappedBus.is_active) {
                const POLYLINE = DecodePolyline(PolylineData, tappedBus.route_id);
                this.setState({ targetBus: tappedBus, decodedPolyline: POLYLINE });
            }
        }
    }
    async onStationMarkerPress(e) {
        const { stationMarkers } = this.state;
        let tappedStation = await stationMarkers.find(function (station) {
            if (station.station_id === e.id)
                return station;
        });
        const routesInStation = await GetRouteInStation(tappedStation.station_id);
        this.setState({ tappedStation: tappedStation, routeInStationData: routesInStation });
        InteractionManager.runAfterInteractions(() => {
            this._stationInfoPanel.show();
        })
    }

    _onStationInfoRowPress(routeID) {
        const POLYLINE = DecodePolyline(PolylineData, routeID);
        this.setState({ decodedPolyline: POLYLINE, targetBus: null });
    }

    _renderStationInfoPanel() {
        const { tappedStation, routeInStationData } = this.state;
        if (routeInStationData.length > 0 && tappedStation !== null) {
            return (
                <StationInfo
                    panelRef={c => this._stationInfoPanel = c}
                    animatedValue={this._stationInfoAnimatedVal}
                    stationName={tappedStation.station_name}
                    routesData={routeInStationData}
                    onPress={this._onStationInfoRowPress}
                />)
        }
    }

    render() {
        const { region } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <NetNotifier />
                <View style={styles.container}>
                    <MapView
                        initialRegion={region}
                        provider={PROVIDER_GOOGLE}
                        ref={(m) => { this._map = m }}
                        showsCompass={false}
                        showsUserLocation={true}
                        followsUserLocation={true}
                        onMapReady={this._onMapReadyInit}
                        style={{ width: '100%', height: '100%' }}
                    >
                        {this._renderStationMarkers()}
                        {this._renderBusMarkers()}
                        {this._renderPolyline()}
                    </MapView>
                    {this._renderStationInfoPanel()}
                </View>
            </View>
        )
    }
}

export default AllBusMap;
