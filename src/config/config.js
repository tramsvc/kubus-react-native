const config = {
    server: {
        'address': '158.108.207.4',
        //'address': '10.0.2.2',
        'connection_timeout': 10000,
    },
    api:{
        'version': 'v1'
    },
    map:{
        'KU_Lat': 14.0236026,
        'KU_Lon': 99.9748993,
        'updateInterval': 2000
    },
    gps:{
        watch_timeout: 5000,
        fastInterval: 3000,
        watch_distance_filter: 5,
        getPositionTimeout: 5000,
        getPositionMaxAge: 5000,
        useHighAccuracy: true
    },
    qr:{
        resumeTimeAfterPause: 2000,
    }
}
export default config;
