import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    notifier_container: {
        flex: 1,
        zIndex: 1,
        position: 'absolute',
        top: 0,
        width: '100%'
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    optionsRightSidepane: {
        flex: 1,
        position: "absolute",
        bottom: '50%',
        right: 10
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 18,
        margin: 10
    }
});

const mapInfoStyles = StyleSheet.create({
    panelContainer: {
        flex: 1,
        backgroundColor: 'white',
    },
    titleContainer: {
        backgroundColor: '#FFF',
        flexDirection: 'row',
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 10,
        alignItems: 'center'
    },
    titleBarText: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    MapInfoContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 5,
    },
    MapInfoColumn: {
        flex: 1
    }
});

export { styles, mapInfoStyles };