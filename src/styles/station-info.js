import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    flex: 1,
});
export const infoBarStyles = StyleSheet.create({
    box: {
        flex: 1,
        marginLeft: 10,
        marginRight:10,
        marginTop:10,
    },
    titleText: {
        fontSize: 20,
        fontWeight: 'bold',
        color:'#fff'
    },
    detailText: {
        fontSize: 18,
        color:'#fff'
    },
    horizontalLine:{
        marginTop: 10,
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor:'#fff'
    }
});