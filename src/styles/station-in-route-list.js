import { StyleSheet } from 'react-native';
import { Platform } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10,
    },
    flatList: {
        flex: 1,
        marginTop: 10
    },
    separator: {
        borderBottomColor: '#bbb',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    emptyTextContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    emptyListText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
        backgroundColor: "#fff",
        overflow: 'scroll',
        ...Platform.select({
            ios: {
                shadowColor: "rgba(0,0,0,0.11)",
                shadowOffset: {
                    width: 1.5,
                    height: 1.5
                },
                shadowRadius: 6,
                shadowOpacity: 1
            },
            android: {
                elevation: 2
            }
        })
    },
    colorContainer: {
        flex: 0,
        padding: 20,
        borderRadius: 30,
        backgroundColor: 'yellow',
    },
    textContainer: {
        margin: 20
    },
    title: {
        fontSize: 16
    }

});

export function colorContainerStyle(color) {
    return {
        alignSelf: 'center',
        height: 50,
        width: 50,
        marginLeft: 5,
        backgroundColor: color
    }
}