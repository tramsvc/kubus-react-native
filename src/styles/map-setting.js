import { Platform, StyleSheet } from 'react-native';

const roundButton = {
    backgroundColor:'#1ebdaf',
    borderColor:'#1ebdaf',
    borderRadius: 40,
    borderWidth:1,
    padding: 10,
    marginTop: 8,
    marginBottom:8
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10
    },
    titleText: {
        fontWeight: 'bold',
        fontSize: 16,
        marginBottom: 10,
    },
    displaySwitch: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 10,
        marginBottom: 5
    },
    picker: {
        height: 50
    },
    mapSettingButton:{
        ...roundButton,
    },
    selectorButton:{
        ...roundButton, 
    }
});

const titleBarStyles = StyleSheet.create({
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 20,
        backgroundColor:'#1ebdaf',
        borderBottomColor: '#1ebdaf',
        borderBottomWidth: 0,
        ...Platform.select({
            ios: {
                shadowColor: "rgba(0,0,0,0.11)",
                shadowOffset: {
                    width: 1.5,
                    height: 1.5
                },
                shadowRadius: 6,
                shadowOpacity: 1
            },
            android: {
                elevation: 2
            }
        })

    },
    closeButtonContainer: {
        paddingRight: 10,
    },
    titleText: {
        flex: 1,
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        paddingLeft: 40,
    }
});

export { styles, titleBarStyles };