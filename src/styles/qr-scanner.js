import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});

const modalStyles = StyleSheet.create({
    panelContainer: {
        flex: 1,
        backgroundColor: 'white',
    },
    titleContainer: {
        flexDirection: 'row',
        padding: 15,
        marginBottom: 10,
        alignItems: 'center',
        backgroundColor: 'rgb(223, 225, 230)'
    },
    titleBarText: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    radioGroup: {
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10
    },
    buttonRow: {
        marginTop: 10,
        padding: 20,
        flexDirection: 'row',
    },
    button: {
        flex: 1,
        marginLeft: 5,
        marginRight: 5
    }
});

export {styles, modalStyles};