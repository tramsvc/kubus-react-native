import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
  image_bg_container: {
    flex: 1,
    resizeMode: 'contain'
  },
  notifier_container:{
    flex: 1, 
    zIndex: 1, 
    position: 'absolute', 
    top: 0, 
    width: '100%'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  logo: {
    alignSelf: 'center',
    width: 140,
    height: 140,
    marginBottom: 10
  },
  title: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  subtitle:{
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  menuBorder: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#1ebdaf',
    borderRadius: 25,
    padding: 16,
    marginTop: 10,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
  },
  menuIcon: {
    marginLeft: 10,
    marginRight: 10,
    color:'#FFF'
  },
  menuTextContainer: {
    flex: 1
  },
  menuText: {
    color:'#FFF',
    //color: '#3A59FF',
    fontWeight: 'bold',
    fontSize: 16
  }
});

export const modalStyles = StyleSheet.create({
  panelContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  titleContainer: {
    flexDirection: 'row',
    padding: 15,
    marginBottom: 15,
    alignItems: 'center',
    backgroundColor: '#1ebdaf'
  },
  titleBarText: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold'
  },
  nearestStationContainer: {
    flex: 0,
    marginLeft: 10,
    marginBottom: 10
  },
  nearestStationText: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  radioGroup: {
    flex: 1,
    marginLeft: 10,
    marginBottom: 10
  },
  buttonRow: {
    marginTop: 10,
    padding: 20,
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5
  }
});